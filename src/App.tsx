import * as React from 'react';
import firebase from 'firebase';
import firebaseConfig from './config/firebase';
import { BrowserRouter as Router} from 'react-router-dom';
import MainRoutes from './config/MainRoutes';
import MainAppBar from './components/AppBar/MainAppBar';
import { ThemeProvider, Box } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';
import { blue, pink } from '@material-ui/core/colors';
//Firebase initialization
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const mainTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: blue,
    secondary: pink,
  },
  typography: {
    fontFamily: ['-apple-system', 'Open Sans', 'sans-serif'].join(','),
  },
});

export default class App extends React.Component {
  render() {
    return (
      <ThemeProvider theme={mainTheme}>
         <Box
          bgcolor='#063852'
          color='#90AFC5'
          style={{ width: '100%', minHeight: '100vh' }}
          id='root-box'
        >
          <Router>
                <MainAppBar/>
                <MainRoutes />
          </Router>
        </Box>
        
      </ThemeProvider>
          
    );
  }
}
