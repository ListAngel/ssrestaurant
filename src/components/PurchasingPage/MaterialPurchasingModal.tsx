import React from 'react';
import { MaterialPurchasing } from '../../model/MaterialPurchasing';
import {
    Modal, Card, Fade, CardContent, CardActions, Button, Typography,
    Table, TableCell, TableHead, TableBody, TableFooter, TablePagination, TableRow,
    Divider, TableContainer
} from '@material-ui/core';

import { renderCurrency, renderTime } from '../../utils/RenderUtils';
import { Purchasing } from '../../model/Purchasing';

interface ModalProps {
    open: boolean;
    order: Purchasing;
    onClose(): void;
}

const MaterialPurchasingModal = (props: ModalProps) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const modalStyle: React.CSSProperties = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100vh',
    };
    const cardStyle: React.CSSProperties = {
        width: '500px',
        outline: 0,
    };

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    const renderTableItem = (items: MaterialPurchasing[]) => {
        if (items.length > 0) {
            const result = items.map((item, k) => {
                return (
                    <TableRow key={k}>
                        <TableCell>{item.menu.name}</TableCell>
                        <TableCell>{item.quantity}</TableCell>
                        <TableCell>{renderCurrency(item.unitPrice)}</TableCell>
                        <TableCell>{renderCurrency(item.quantity * item.unitPrice)}</TableCell>
                    </TableRow>
                );
            });
            return result;
        } else {
            return null;
        }
    };

    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <Fade in={props.open}>
                <Card style={cardStyle}>
                    <CardContent>
                        <Typography component='h2' variant='h5'>
                            Purchasing Detail
                        </Typography>
                        <Typography variant='body2'>{`ID: ${props.order.id}`}</Typography>
                        <Typography variant='body2'>{`Time: ${renderTime(
                        props.order.date
                        )}`}</Typography>
                        <Divider style={{ margin: '8px 0px' }} />
                        <Typography component='h2' variant='h6'>
                            Material Purchasing
                        </Typography>
                        <TableContainer>
                            <Table size='small'>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Quantity</TableCell>
                                        <TableCell>Unit Price</TableCell>
                                        <TableCell>Total</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>{renderTableItem(props.order.materialPurchasing)}</TableBody>
                                <TableFooter>
                                    <TableRow>
                                        <TableCell colSpan={4}>
                                        <TablePagination
                                            rowsPerPageOptions={[5, 10, 25]}
                                            component='div'
                                            count={props.order.materialPurchasing.length}
                                            rowsPerPage={rowsPerPage}
                                            page={page}
                                            onChangePage={handleChangePage}
                                            onChangeRowsPerPage={handleChangeRowsPerPage}
                                        />
                                        </TableCell>
                                    </TableRow>
                                </TableFooter>
                            </Table>
                        </TableContainer>
                    </CardContent>
                    <CardActions>
                        <Button variant='text' onClick={props.onClose} style={{ marginLeft: 'auto' }}>
                        <b>Close</b>
                        </Button>
                    </CardActions>
                </Card>
            </Fade>

        </Modal>
    )

}


export default MaterialPurchasingModal;