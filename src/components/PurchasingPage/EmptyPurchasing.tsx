import React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';
import { useHistory } from 'react-router-dom';


const EmptyPurchasing = () => {
    const history = useHistory();

    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
    };
    
    const createMenu = () => {
      history.push('/admin/purchasing/create');
    }
  
  return (
    <Container style={containerStyle}>
      <Card style={{ maxWidth: 300 }}>
        <CardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            Restaurant Purchasing
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            Add an Purchasing for Your Restaurant
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size='small'
            color='primary'
            style={{ fontWeight: 'bold', marginLeft: 'auto' }}
            onClick={createMenu}
          >
            Create
          </Button>
        </CardActions>
      </Card>
    </Container>

  )

}



export default EmptyPurchasing;