import React from 'react';
import { Purchasing } from '../../model/Purchasing';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import {deletePurchasing} from '../../controller/PurchasingController';


interface purchasingProps {
    open: boolean;
    onClose(open: boolean): void;
    purchasingDeleted: Purchasing;
}
const DeletePurchasingModal = (props: purchasingProps) => {
    const [loading, setLoading] = React.useState(false);

    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (purchasingDelete: Purchasing) => {
        setLoading(true);
        const check = await deletePurchasing(purchasingDelete);
        if(check === true){
            setLoading(false);
            window.location.reload();
        }else{
            window.alert(`This Purchasing can't deleted`);
        } 
    }

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Purchasing"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.purchasingDeleted.id} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleDelete(props.purchasingDeleted)}} color="primary">
                    Yes
                </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
                </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}

export default DeletePurchasingModal;