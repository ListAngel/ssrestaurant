import React from 'react';
import {
    Container,
    Typography,
    TextField,
    Button,
} from "@material-ui/core";
import { Add } from '@material-ui/icons';
import CancelIcon from '@material-ui/icons/Cancel';
import { Theme, makeStyles } from "@material-ui/core/styles";
import { MaterialPurchasing } from '../../model/MaterialPurchasing';
import { Purchasing } from '../../model/Purchasing';
import { updatePurchasingItem, getPurchasingWithId } from '../../controller/PurchasingController';
import PurchasingTable from './PurchasingTable';
import AddMenuModal from '../../components/MenuPage/AddMenuModal';
import { Menu } from '../../model/Menu';
import { getAllMenu } from '../../controller/MenuController';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import { useHistory, useParams } from 'react-router-dom';


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: '100%',
        width: '75%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'start',
        alignItems: 'stretch',
        marginTop: theme.spacing(5),
    },
    form: {
        width: '80%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        // flexWrap: 'wrap',
        padding: '16px 0',
    },
    buttonStyle: {
        width: '30%',
        backgroundColor: '#0063B2FF',
        color: '#9CC3D5FF',
        marginTop: theme.spacing(3),
    },
    buttonEnd:{
        alignSelf: 'center',
        marginTop: theme.spacing(3),
    }
}));

const EditPurchasing = () => {
    const classes = useStyles();
    const [idPurchasing, setIdPurchasing] = React.useState('');
    const [vendorName, setVendorName] = React.useState('');
    const [materialPurchasing, setMaterialPurchasing] = React.useState<MaterialPurchasing[]>([]); //Orders that added in the table.
    const [modalOpen, setModalOpen] = React.useState(false);
    const [menuList, setMenuList] = React.useState<Menu[]>([]); //menuList is all loaded menu from db, which will be shown in AddMenuModal
    const [loading, setLoading] = React.useState(false);
    const history = useHistory();
    const { id } = useParams();

    async function fetchMenu(){
        const result = await getAllMenu();
        setMenuList(result);
    }

    const fetchPurchasing = async (purcasingId: string) =>{
        await getPurchasingWithId(purcasingId).then((doc)=>{
            setIdPurchasing(purcasingId);
            setVendorName(doc.vendorName);
            setMaterialPurchasing(doc.materialPurchasing);

        })
    }
    React.useEffect(() => {
        fetchMenu();
        fetchPurchasing(id || 'null');
        // eslint-disable-next-line
    }, []);

    if (menuList.length < 1) {
        return <FullScreenSpinner open={true} />;
    }

    const removeMenu = (item: MaterialPurchasing) => {
        const temp = [...materialPurchasing];
        const index = temp.indexOf(item);
        if (index !== -1) {
            temp.splice(index, 1);
            setMaterialPurchasing(temp);
        }
    };

    const handleQtyChange = (ev: any, item: MaterialPurchasing) => {
        const temp = [...materialPurchasing];
        const index = temp.indexOf(item);
        temp[index].quantity = ev.target.value as number;

        setMaterialPurchasing(temp);
    };

    const handlePriceChange = (ev: any, item: MaterialPurchasing) => {
        const temp = [...materialPurchasing];
        const index = temp.indexOf(item);
        temp[index].unitPrice = ev.target.value as number;

        setMaterialPurchasing(temp);
    };
    //Add selected menu from modal into table (state items)
    const addSelectedMenu = (item: Menu) => {
        // const temp = [...items, item];
        // setItems(temp);
        const newMaterial: MaterialPurchasing = {
            menu: item,
            quantity: 1,
            unitPrice: item.price,
        };
        const temp = [...materialPurchasing, newMaterial];
        setMaterialPurchasing(temp);
    };
    const addMaterial = () => {
        setModalOpen(true);
        const mOrders: { menu: Menu; quantity: number , unitPrice: number}[] = [];
         materialPurchasing.forEach((item)=>{
            mOrders.push({
                 menu: item.menu,
                 quantity: item.quantity,
                 unitPrice: item.unitPrice,
             })
        });

    }

    const btnGroupStyle: React.CSSProperties = {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',  
    };

    const editPurchasingOrder = async () => {
        setLoading(true);

        const mOrders: { menu: Menu; quantity: number, unitPrice: number }[] = [];
        const today = new Date();
        let tempTotal = 0;

        materialPurchasing.forEach((item)=>{
            tempTotal = tempTotal + (item.unitPrice*item.quantity);
            mOrders.push({
                menu: item.menu,
                quantity: Number(item.quantity),
                unitPrice: Number(item.unitPrice),
            })
        })
        
        const newPurchasing: Purchasing ={
            vendorName: vendorName,
            materialPurchasing: mOrders,
            status: "process",
            total: Number(tempTotal),
            date: today.getTime()
            
        }
        const checker = await updatePurchasingItem(idPurchasing,newPurchasing);
        if(checker === true){
            setLoading(false);
            history.push('/admindashboard');
        }else {
            setLoading(false);
            window.alert(`Can't Create Purchasing`);
        }
    }

    return (
        <Container className={classes.root}>
            <div style={{ flexGrow: 1 }}>
                <Typography variant='h4'>
                    Edit Purchasing
                </Typography>
            </div>
            <form className={classes.form}>
                <TextField
                    fullWidth
                    label='Vendor Name'
                    variant='outlined'
                    value={vendorName}
                    onChange={(ev) => setVendorName(ev.target.value)}
                />
                <Button
                    variant="text"
                    startIcon={<Add />}
                    disableRipple
                    className={classes.buttonStyle}
                    onClick={addMaterial}
                >
                    <b>Add Material</b>
                </Button>
                <PurchasingTable
                    materialPurchasing={materialPurchasing}
                    onDelete={(item) => removeMenu(item)}
                    onQtyChange={(ev, item) => handleQtyChange(ev, item)}
                    onPriceChange={(ev, item) => handlePriceChange(ev, item)}
                />
                <AddMenuModal
                    open={modalOpen}
                    menuList={menuList}
                    onClose={() => {
                        setModalOpen(false);
                    }}
                    onMenuAdd={(item) => {
                        addSelectedMenu(item);
                    }}
                />
                 <div style={btnGroupStyle}>
                <Button
                    variant='text'
                    color='secondary'
                    startIcon={<CancelIcon />}
                    disableRipple
                    className={classes.buttonEnd}
                    onClick={() => {
                        history.push('/admindashboard');
                    }}
                >
                    <b>Cancel</b>
                </Button>
                <Button
                    variant='text'
                    color='primary'
                    startIcon={<Add />}
                    disableRipple
                    className={classes.buttonEnd}
                    disabled={
                        materialPurchasing.length < 1 ||
                        vendorName.length < 3
                    }
                    onClick={editPurchasingOrder}
                >   
                    <b>Edit Purchasing Order</b>
                </Button>
            </div>
            </form>
           
            <FullScreenSpinner open={loading} />
        </Container>
    )
}

export default EditPurchasing;