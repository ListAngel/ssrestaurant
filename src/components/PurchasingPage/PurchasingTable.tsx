import React from 'react';
import { MaterialPurchasing } from '../../model/MaterialPurchasing';
import NumberFormat from "react-number-format";
import {
  TableRow,
  TableCell,
  TextField,
  IconButton,
  Typography,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { renderCurrency } from '../../utils/RenderUtils';

interface TableProps {
  materialPurchasing: MaterialPurchasing[];
  onDelete(item: MaterialPurchasing): void;
  onQtyChange(ev: any, item: MaterialPurchasing): void;
  onPriceChange(ev: any, item: MaterialPurchasing): void;
}
interface NumberFormatCustomProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function NumberFormatCustom(props: NumberFormatCustomProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      prefix="Rp. "
    />
  );
}
const PurchasingTable = (props: TableProps) => {
  const placeholderStyle = {
    margin: '0 auto',
    padding: '16px',
  };

  const deleteItem = (item: MaterialPurchasing) => {
    props.onDelete(item);
  };

  const handleQuantityChange = (ev: any, item: MaterialPurchasing) => {
    props.onQtyChange(ev, item);
  };
  
  const handlePriceChange = (ev: any, item: MaterialPurchasing) => {
    props.onPriceChange(ev, item);
  };

  const renderPrice = (item: MaterialPurchasing) => {
    return item.quantity * item.unitPrice;
  };

  const renderGrandTotal = (item: MaterialPurchasing[]) => {
    let result = 0;
    item.forEach((item) => {
      result = result + item.quantity * item.unitPrice
    });
    return result;
  };
  const renderMaterialPurchasingItems = (items: MaterialPurchasing[]) => {
    return items.map((item, k) => {
      return (
        <TableRow key={k}>
          <TableCell style={{ width: 60 }}>
            <IconButton
              aria-label='Delete'
              style={{ width: 50 }}
              onClick={() => deleteItem(item)}
            >
              <Delete fontSize='inherit' />
            </IconButton>
          </TableCell>
          <TableCell>{item.menu.name}</TableCell>
          <TableCell>
            <TextField
              type='number'
              InputProps={{ inputProps: { min: 1, max: 10 } }}
              style={{ width: 75 }}
              value={item.quantity}
              onChange={(ev) => handleQuantityChange(ev, item)}
            />
          </TableCell>
          <TableCell>
            <TextField
              style={{ width: 100 }}
              value={item.unitPrice}
              onChange={(ev) => handlePriceChange(ev, item)}
              InputProps={{
                inputComponent: NumberFormatCustom as any
              }}
            />
          </TableCell>
          <TableCell>{renderCurrency(renderPrice(item))}</TableCell>
        </TableRow>
      )
    })

  }
  const renderTableBody = (items: MaterialPurchasing[]) => {
    if (items.length < 1 || items === undefined) {
      return (
        <TableRow style={placeholderStyle}>
          <TableCell colSpan={6}>
            <Typography align='center' variant='body2' color='textSecondary'>
              <i>Please add a Material</i>
            </Typography>
          </TableCell>
        </TableRow>
      );
    } else {
      return renderMaterialPurchasingItems(items);
    }
  };

  return (
    <TableContainer component={Paper}>
      <Table size='small'>
        <TableHead>
          <TableRow>
            <TableCell>
              <b>Action</b>
            </TableCell>
            <TableCell>
              <b>Material Name</b>
            </TableCell>
            <TableCell>
              <b>Quantity</b>
            </TableCell>
            <TableCell>
              <b>Unit Price</b>
            </TableCell>
            <TableCell>
              <b>Total</b>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {renderTableBody(props.materialPurchasing)}
          <TableRow >
            <TableCell colSpan={4}>
              <Typography align='right' variant='body2'>
                <b>Grand Total</b>
              </Typography>
            </TableCell>
            <TableCell>
              <b>{renderCurrency(renderGrandTotal(props.materialPurchasing))}</b>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default PurchasingTable;