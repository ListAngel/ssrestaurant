import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
    TableContainer,
    Paper,
    IconButton,
    Table,
    TableBody,
    TableHead,
    Tooltip,
    TableRow,
    TableCell,
    Button,
    Container,
    Typography,
    TextField,
    Select,
    MenuItem
} from '@material-ui/core';
import { Purchasing } from '../../model/Purchasing';
import { getAllPurchasing } from '../../controller/PurchasingController';
import { useHistory } from 'react-router-dom';
import EmptyPurchasing from './EmptyPurchasing';
import ViewListIcon from '@material-ui/icons/ViewList';
import UpdateIcon from '@material-ui/icons/Update';
import CachedIcon from '@material-ui/icons/Cached';
import DeleteIcon from '@material-ui/icons/Delete';
import { Add } from '@material-ui/icons';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DeletePurchasingModal from './DeletePurchasingModal';
import ReceiptOfGoodModal from './ReceiptOfGoodModal';
import MaterialPurchasing from './MaterialPurchasingModal';

import { renderTime, renderCurrency } from '../../utils/RenderUtils';

const StyledTableCell = withStyles(() => ({
    head: {
        backgroundColor: 'gray',
        color: 'white',
        fontSize: 20,
    },
}))(TableCell);

const TableHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell style={{ width: 200, textAlign: 'center' }}>
                    Actions
                </StyledTableCell>
                <StyledTableCell>Date </StyledTableCell>
                <StyledTableCell>Vendor Name</StyledTableCell>
                <StyledTableCell>Total</StyledTableCell>
                <StyledTableCell>Status</StyledTableCell>
            </TableRow>
        </TableHead>
    );
};

const useStyles = makeStyles(theme => ({
    root: {
        paddingBottom: theme.spacing(8),
    },
    paper: {
        marginTop: theme.spacing(5),
    },
    done: {
        color: '#64dd17',
        marginLeft: theme.spacing(2),

    },
    buttonIcon: {
        marginLeft: theme.spacing(7),
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select: {
        height: 55,
        marginLeft: theme.spacing(3),

    }
}))

const ShowPurchasing = () => {
    const defaultPurchasing: Purchasing = {
        id: 'null',
        vendorName: "null",
        materialPurchasing: [],
        status: "",
        date: 0,
        total: 0,
    }

    const [purchasingSelected, setPurchasingSelected] = React.useState<Purchasing>(defaultPurchasing);
    const history = useHistory();
    const classes = useStyles();
    const [purchasing, setPutchasing] = React.useState([] as any[]);
    const [filterPurchasing, setFilterPurchasing] = React.useState([] as any[]);
    const [tempFilterPurchasing, setTempFilterPurchasing] = React.useState([] as any[]);
    const [loading, setLoading] = React.useState(true);
    const [txtSearch, setTxtSearch] = React.useState('');
    const [status, setStatus] = React.useState('All');

    const [modalDelete, setModalDelete] = React.useState(false);
    const [modalDetailPurchasing, setModalDetailPurchasing] = React.useState(false);
    const [modalProcess, setModalProcess] = React.useState(false);

    const createPruchasing = () => {
        history.push('/admin/purchasing/create');
    }
    const showPurchasingModal = (item: Purchasing) => {
        setPurchasingSelected(item);
        setModalDetailPurchasing(true);
    }
    const updatePurchasingModal = (item: Purchasing) => {
        history.push(`/admin/purchasing/edit/${item.id}`);
    }
    const deletePurchasingModal = (item: Purchasing) => {
        setPurchasingSelected(item);
        setModalDelete(true);
    }

    const showProcessModal = (item: Purchasing) => {
        setPurchasingSelected(item);
        setModalProcess(true);
    }

    const renderTableBody = (items: Purchasing[]) => {
        const tableBody = items.map((item: Purchasing, k) => {
            return (
                <TableRow hover tabIndex={-1} key={item.id}>
                    <TableCell>
                        
                        {item.status === 'process' ? (
                            <div>
                                <Tooltip title='View Purchasing' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            showPurchasingModal(item);
                                        }}
                                    >
                                        <ViewListIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title='Update Purchasing' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            updatePurchasingModal(item);
                                        }}
                                    >
                                        <UpdateIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title='Delete Purchasing' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            deletePurchasingModal(item);
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </Tooltip>
                            </div>
                            
                        ) : (
                            <Tooltip title='View Purchasing' arrow className={classes.buttonIcon}>
                                <IconButton
                                    aria-label='List'
                                    style={{ width: 50 }}
                                    onClick={() => {
                                        showPurchasingModal(item);
                                    }}
                                >
                                    <ViewListIcon />
                                </IconButton>
                            </Tooltip>
                        )}

                    </TableCell>

                    <TableCell>{renderTime(item.date)}</TableCell>
                    <TableCell>{item.vendorName}</TableCell>
                    <TableCell>{renderCurrency(item.total)}</TableCell>
                    <TableCell>
                        {item.status === 'process' ? (
                            <Tooltip title='Receipt Of Goods' arrow>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    startIcon={<CachedIcon />}
                                    onClick={() => {
                                        showProcessModal(item);
                                    }}
                                >
                                    {item.status}
                                </Button>
                            </Tooltip>

                        ) : (
                                <div className={classes.done}>
                                    <CheckCircleOutlineIcon />
                                    <Typography>
                                        Done
                                    </Typography>
                                </div>
                            )}
                    </TableCell>


                </TableRow>
            )
        })
        return tableBody;
    }
    // Get All Promo With Contoller Promo
    React.useEffect(() => {
        getAllPurchasing().then(result => {
            setPutchasing(result);
            setTempFilterPurchasing(result);
            setFilterPurchasing(result);
            setLoading(false);
        })
    }, []);

    if (purchasing.length < 1 && loading === false) {
        return (
            <div>
                <EmptyPurchasing />
            </div>
        );
    }
    const handleSearchPurchasing = (ev: any) => {
        setTxtSearch(ev.target.value);

        const temp = tempFilterPurchasing.filter((item: any) =>
            item.vendorName.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterPurchasing(temp);

    };
    const handleStatusActive = (ev: any) => {
        setStatus(ev.target.value);

        if (ev.target.value === "All") {
            setFilterPurchasing(purchasing);
            setTempFilterPurchasing(purchasing);
        } else if (ev.target.value === "Process") {
            const temp = purchasing.filter((item: any) => {
                return (item.status === 'process')
            });
            setFilterPurchasing(temp);
            setTempFilterPurchasing(temp);
        } else {
            const temp = purchasing.filter((item: any) => {
                return item.status === 'done'
            });
            setFilterPurchasing(temp);
            setTempFilterPurchasing(temp);
        }
    }
    const handleCloseModal = () => {
        setModalDetailPurchasing(false);
    };
    return (
        <Container maxWidth="md" className={classes.root} >
            <Typography variant='h4' component='h4'>
                Purchasing Management
            </Typography>
            <form>
                <TextField
                    className={classes.search}
                    label='Search Purchasing'
                    variant='outlined'
                    value={txtSearch}
                    onChange={(ev) => handleSearchPurchasing(ev)}
                ></TextField>
                <Select
                    className={classes.select}
                    style={{ marginTop: 20 }}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={status}
                    onChange={handleStatusActive}
                >
                    <MenuItem value={"All"}>All</MenuItem>
                    <MenuItem value={"Done"}>Done</MenuItem>
                    <MenuItem value={"Process"}>On Process</MenuItem>
                </Select>
                <Button
                    variant='contained'
                    startIcon={<Add />}
                    color='primary'
                    style={{ height: 55, marginLeft: 20 }}
                    onClick={createPruchasing}
                >
                    Add New Pruchasing
                </Button>
            </form>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table>
                        <TableHeader />
                        <TableBody>{renderTableBody(filterPurchasing)}</TableBody>
                    </Table>
                </TableContainer>
            </Paper>

            <DeletePurchasingModal
                open={modalDelete}
                purchasingDeleted={purchasingSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}

            />
            <ReceiptOfGoodModal
                open={modalProcess}
                purchasingSelected={purchasingSelected}
                onClose={(open) => {
                    setModalProcess(open)
                }}
            />
            <MaterialPurchasing
                open={modalDetailPurchasing}
                order={purchasingSelected}
                onClose={() => {
                    handleCloseModal();
                }}
            />
        </Container>



    )
}


export default ShowPurchasing;