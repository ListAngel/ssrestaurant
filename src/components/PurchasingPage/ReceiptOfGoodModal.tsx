import React from 'react';
import { Purchasing } from '../../model/Purchasing';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import {receiptGoodPruchasing} from '../../controller/PurchasingController';

interface promoProps {
    open: boolean;
    onClose(open: boolean): void;
    purchasingSelected: Purchasing;
}

const ReceiptOfGoodModal = (props: promoProps) => {
    const [loading, setLoading] = React.useState(false);
    const handleClose = () => {
        props.onClose(false);
    };


    const handleROG = async (purchasingSelected: Purchasing) => {
        setLoading(true);
        const checker = await receiptGoodPruchasing(purchasingSelected);
        console.log(checker, purchasingSelected);
        if(checker === true){
            window.location.reload();
            setLoading(false);
        }else {
            setLoading(false);
            window.alert(`Can't Create Purchasing`);
        }
    }
    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Purchasing"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    Has the all item been received {props.purchasingSelected.id} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleROG(props.purchasingSelected)}} color="primary">
                    Yes
                </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
                </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}

export default ReceiptOfGoodModal;