import React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';

import CreateTablesModal from './CreateTablesModal';

const EmptyTables = () => {
    const [modalCreate, setModalCreate] = React.useState(false);
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
    };
    const createTables = () => {
        setModalCreate(true);
    }
    return (
        <Container style={containerStyle}>
          <Card style={{ maxWidth: 300 }}>
            <CardContent>
              <Typography gutterBottom variant='h5' component='h2'>
                Restaurant Tables
              </Typography>
              <Typography variant='body2' color='textSecondary' component='p'>
                Add an Table for Your Restaurant
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                size='small'
                color='primary'
                style={{ fontWeight: 'bold', marginLeft: 'auto' }}
                onClick={createTables}
              >
                Create
              </Button>
            </CardActions>
          </Card>
          <CreateTablesModal
            open={modalCreate}
            onClose={() => {
                setModalCreate(false);
            }}
    
          />
        </Container>
    )
}


export default EmptyTables;