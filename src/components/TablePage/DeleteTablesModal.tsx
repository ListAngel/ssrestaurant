import React from 'react';
import { Tables } from '../../model/Tables';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import {deleteTable} from '../../controller/TableController';

interface tableProps {
    open: boolean;
    onClose(open: boolean): void;
    tableDeleted: Tables;
}
const DeletesPromoModal = (props: tableProps) => {
    const [loading, setLoading] = React.useState(false);

    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (tableDeleted: Tables) => {
        setLoading(true);
        await deleteTable(tableDeleted);
        setLoading(false);
        window.location.reload();
    }

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Promo"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.tableDeleted.tableNumber} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleDelete(props.tableDeleted)}} color="primary">
                    Yes
                </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
                </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}

export default DeletesPromoModal;