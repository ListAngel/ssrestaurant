import React from 'react';
import {
    Container,
    Typography,
    TextField,
    Button,
    Modal,
    Select,
    MenuItem,
} from "@material-ui/core";
import { Add } from '@material-ui/icons';
import CancelIcon from '@material-ui/icons/Cancel';
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import { createTable } from '../../controller/TableController';
import { Tables } from '../../model/Tables';

interface ModalProps {
    open: boolean;
    onClose(): void;

}
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),
            backgroundColor: '#063852',
            color: '#90AFC5'
        },
        formStyle: {
            paddingTop: theme.spacing(2)
        },
        buttonStyle: {
            marginTop: theme.spacing(10),
            alignSelf: 'center',
        },

    })
);
const CreateTablesModal = (props: ModalProps) => {
    const classes = useStyles();
    const [loading, setLoading] = React.useState(false);
    const [inputTableNumber, setInputTableNumber] = React.useState('');
    const [tableType, setTableType] = React.useState('');
    const [tableCapacity, setTableCapacity] = React.useState(0);
    const [tableStatus, setTableStaus] = React.useState('');

    const modalStyle = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    };
    const handleSelectChange = (ev: any) => {
        setTableType(ev.target.value);
        setTableStaus("Available");

        if(ev.target.value === 'Small'){
            setTableCapacity(2);
        }else if (ev.target.value === "Medium"){
            setTableCapacity(4)
        }else{
            setTableCapacity(6);
        }
    }
    const handleCancel = () => {
        props.onClose();
    }
    const handleAddTable = async () => {
        setLoading(true);
        if (inputTableNumber === "" || tableType === "" ){
            alert("please fill in all fields!!");
        }else{
            const newTable: Tables = {
                tableNumber: inputTableNumber,
                type: tableType,
                capacity: tableCapacity,
                status: tableStatus,
            }
            const checkCreateTable = await createTable(newTable);
            if(checkCreateTable === true){
                setLoading(false);
                window.location.reload();
              }else{
                setLoading(false);
                window.alert(`This Promo is Already`);
              
              }
        }
    }
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <Container maxWidth="sm" className={classes.root}>
                <Typography variant="h4">Create Table</Typography>
                <form className={classes.formStyle}>
                    <TextField
                        variant='outlined'
                        label='Table Number'
                        style={{ marginBottom: 20, width: 500 }}
                        value={inputTableNumber}
                        onChange={(ev) => setInputTableNumber(ev.target.value)}
                    />
                    <Select
                        style={{ marginBottom: 20, width: 500 }}
                        labelId="demo-simple-select-helper-label"
                        id="demo-simple-select-helper"
                        value={tableType}
                        onChange={handleSelectChange}
                    >
                        <MenuItem value={"Small"}>Small</MenuItem>
                        <MenuItem value={"Medium"}>Medium</MenuItem>
                        <MenuItem value={"Big"}>Big</MenuItem>
                    </Select>
                    <Typography  style={{ marginBottom: 20, width: 500 }} gutterBottom>Capacity: {tableCapacity}</Typography>
                    <Typography  style={{ marginBottom: 20, width: 500 }} gutterBottom>Status: {tableStatus}</Typography>
                    <Button
                        variant='text'
                        color='secondary'
                        startIcon={<CancelIcon />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={handleCancel}
                    >
                        <b>Cancel</b>
                    </Button>
                    <Button
                        variant='text'
                        color='primary'
                        startIcon={<Add />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={handleAddTable}
                    >
                        <b>ADD Table</b>
                    </Button>
                </form>
                <FullScreenSpinner open={loading} />
            </Container>
        </Modal>
    )
}


export default CreateTablesModal;