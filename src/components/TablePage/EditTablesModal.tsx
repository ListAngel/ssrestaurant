import React from 'react'
import {
    Container,
    Typography,
    TextField,
    Button,
    Select,
    MenuItem,
} from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { useHistory, useParams } from 'react-router-dom';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import UpdateIcon from '@material-ui/icons/Update';
import {Tables} from '../../model/Tables';
import { getTableWithId, updateTable } from '../../controller/TableController';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),
            backgroundColor: '#063852',
            color: '#90AFC5'
        },
        formStyle: {
            paddingTop: theme.spacing(2)
        },
        buttonStyle: {
            marginTop: theme.spacing(10),
            alignSelf: 'center',
        },

    })
);
const EditTablesModal = () => {
    const classes = useStyles();
    const [loading, setLoading] = React.useState(false);
    const [tableId, setTableId] = React.useState('');
    const [inputTableNumber, setInputTableNumber] = React.useState('');
    const [tableType, setTableType] = React.useState('');
    const [tableCapacity, setTableCapacity] = React.useState(0);
    const [tableStatus, setTableStaus] = React.useState('');
    const { id } = useParams();
    const history = useHistory();

    const fetchTable = async (tableId: string) => {
       await getTableWithId(tableId).then(result => {
           setTableId(tableId);
           setInputTableNumber(result.tableNumber);
           setTableCapacity(result.capacity);
           setTableType(result.type);
           setTableStaus(result.status);
       })
    }
    React.useEffect(() => {
        fetchTable(id || 'null');
        // eslint-disable-next-line
    }, []);

    const handleSelectChange = (ev: any) => {
        setTableType(ev.target.value);
        setTableStaus("Available");

        if(ev.target.value === 'Small'){
            setTableCapacity(2);
        }else if (ev.target.value === "Medium"){
            setTableCapacity(4)
        }else{
            setTableCapacity(6);
        }
    }
    
    const handleCancel = () => {
        history.push('/admindashboard');
    }

    const handleEditTable = async () => {
        setLoading(true);
        if (inputTableNumber === "" || tableType === "" ){
            alert("please fill in all fields!!");
        }else{
            const newTable: Tables = {
                tableNumber: inputTableNumber,
                type: tableType,
                capacity: tableCapacity,
                status: tableStatus,
            }
            const checkEditTable = await updateTable(tableId,newTable);

            if(checkEditTable === true){
                setLoading(false);
                history.push('/admindashboard');
            }else {
                setLoading(false);
                window.alert(`This Table is Already`);
            }
        }

    }

    return (
        <Container maxWidth="sm" className={classes.root}>
        <Typography variant="h4">Create Table</Typography>
        <form className={classes.formStyle}>
            <TextField
                variant='outlined'
                label='Table Number'
                style={{ marginBottom: 20, width: 500 }}
                value={inputTableNumber}
                onChange={(ev) => setInputTableNumber(ev.target.value)}
            />
            <Select
                style={{ marginBottom: 20, width: 500 }}
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={tableType}
                onChange={handleSelectChange}
            >
                <MenuItem value={"Small"}>Small</MenuItem>
                <MenuItem value={"Medium"}>Medium</MenuItem>
                <MenuItem value={"Big"}>Big</MenuItem>
            </Select>
            <Typography  style={{ marginBottom: 20, width: 500 }} gutterBottom>Capacity: {tableCapacity}</Typography>
            <Typography  style={{ marginBottom: 20, width: 500 }} gutterBottom>Status: {tableStatus}</Typography>
            <Button
                variant='text'
                color='secondary'
                startIcon={<CancelIcon />}
                disableRipple
                className={classes.buttonStyle}
                onClick={handleCancel}
            >
                <b>Cancel</b>
            </Button>
            <Button
                variant='text'
                color='primary'
                startIcon={<UpdateIcon />}
                disableRipple
                className={classes.buttonStyle}
                onClick={handleEditTable}
            >
                <b>Update Table</b>
            </Button>
        </form>
        <FullScreenSpinner open={loading} />
    </Container>
    )
}


export default EditTablesModal;