import React from 'react';
import {Tables} from '../../model/Tables';
import {getAllTable} from '../../controller/TableController';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import {
    TableContainer,
    Paper,
    Table,
    TableBody,
    TableHead,
    TableRow,
    TableCell,
    Button,
    Container,
    Typography,
    TextField,
    Select,
    MenuItem,
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import EmptyTables from './EmptyTables';
import CreateTablesModal from './CreateTablesModal';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import DeleteTablesModal from './DeleteTablesModal';
const useStyles = makeStyles(theme => ({
    root: {
        paddingBottom: theme.spacing(8),
    },
    paper: {
        marginTop: theme.spacing(5),
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select:{
        height: 55,
        marginLeft: theme.spacing(3),

    }
}))

const StyledTableCell = withStyles(() => ({
    head: {
        backgroundColor: 'gray',
        color: 'white',
        fontSize: 20,
    },
}))(TableCell);

const TableHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell style={{ width: 200, textAlign: 'center' }}>
                    Actions
                </StyledTableCell>
                <StyledTableCell>Table Number</StyledTableCell>
                <StyledTableCell>Type</StyledTableCell>
                <StyledTableCell>Capacity</StyledTableCell>
                <StyledTableCell>Status</StyledTableCell>
            </TableRow>
        </TableHead>
    );
};

const ShowTable = () => {
    const history = useHistory();
    const classes = useStyles();
    const [txtSearch, setTxtSearch] = React.useState('');
    const [tables, setTables] = React.useState([] as any[]);
    const [modalCreate, setModalCreate] = React.useState(false);
    const [modalDelete, setModalDelete] = React.useState(false);
    const [loading, setLoading] = React.useState(true);
    const [status, setStatus] = React.useState('All');
    const [filterTables, setFilterTables] = React.useState([] as any[]);
    const [tempFilterTable, setTempFilterTable] = React.useState([] as any[]);
    const defaultTableSelected: Tables = {
        id: '',
        tableNumber: '',
        type: '',
        capacity: 0,
        status: '',
    }
    const [tableSelected, setTableSelected] = React.useState<Tables>(defaultTableSelected);

    React.useEffect(() => {
        getAllTable().then((result) => {
            setTables(result);
            setFilterTables(result);
            setTempFilterTable(result);
            setLoading(false);
        })
    }, []);

    if (tables.length < 1 && loading === false) {
        return (
            <div>
                <EmptyTables />
            </div>
        );
    }
    const handleUpdatePromo = (item: Tables) => {
        history.push(`/admin/tables/edit/${item.id}`);
    }

    const handleDeletePromo = async (promo: Tables) => {
        setTableSelected(promo);
        setModalDelete(true);
    }
    
    const renderTableBody = (items: Tables[]) => {
        const result = items.map((item: Tables, k) => {
            return(
                <TableRow key={k}>
                <TableCell style={{ textAlign: 'center' }}>
                    <Button color='primary' onClick={() => handleUpdatePromo(item)}>
                            <EditIcon />
                    </Button>{' '}
                             |
                    <Button color='secondary' onClick={() => handleDeletePromo(item)}>
                            <DeleteIcon />
                    </Button>
                    </TableCell>
                    <TableCell>{item.tableNumber}</TableCell>
                    <TableCell>{item.type}</TableCell>
                    <TableCell>{item.capacity}</TableCell>
                    <TableCell>{item.status}</TableCell>   
            </TableRow>
            );
           
        });
        return result
    }
 
    const createTable = () => {
        setModalCreate(true);
    }
    const handleSearchPromo = (ev: any) => {
        setTxtSearch(ev.target.value);
        
        const temp = tempFilterTable.filter((item: any) =>
            item.tableNumber.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterTables(temp);
         
    };
    const handleStatusActive = (ev: any) => {
        setStatus(ev.target.value);
        if(ev.target.value === "All"){
            setFilterTables(tables);
            setTempFilterTable(tables);
        }else if(ev.target.value === "Available"){
            const temp = tables.filter((item: any) => {
                return item.status === "Available"
            });
            setFilterTables(temp);
            setTempFilterTable(temp);
        }else {
            const temp = tables.filter((item: any) => {
                return item.status === "UnAvailable"
            });
            setFilterTables(temp);
            setTempFilterTable(temp);
        }  
    }
    return (
        <Container maxWidth="md" className={classes.root} >
            <Typography variant='h4' component='h4'>
                Tables Management
            </Typography>
            <form>
                <TextField
                    className={classes.search}
                    label='Search Table'
                    variant='outlined'
                    value={txtSearch}
                    onChange={(ev) => handleSearchPromo(ev)}
                ></TextField>
                 <Select
                    className = {classes.select}
                    style={{ marginTop: 20 }}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={status}
                    onChange={handleStatusActive}
                >
                    <MenuItem value={"All"}>All</MenuItem>
                    <MenuItem value={"Available"}>Available</MenuItem>
                    <MenuItem value={"UnAvailable"}>UnAvailable</MenuItem>
                </Select>
                 <Button
                    variant='contained'
                    startIcon={<Add />}
                    color='primary'
                    style={{ height: 55, marginLeft: 20 }}
                    onClick={createTable}
                >
                    Add New Table
                </Button>
            </form>
            
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table>
                        <TableHeader />
                        <TableBody>{renderTableBody(filterTables)}</TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <CreateTablesModal
                open={modalCreate}
                onClose={() => {
                    setModalCreate(false);
                }}

            />
            <DeleteTablesModal
                open={modalDelete}
                tableDeleted={tableSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}

            />
            <FullScreenSpinner open={loading} />
        </Container>
    )

}


export default ShowTable;