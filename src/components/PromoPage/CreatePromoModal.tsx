import React from 'react';
import {
    Container,
    Typography,
    TextField,
    Button,
    Modal,
} from "@material-ui/core";
import { Add } from '@material-ui/icons';
import CancelIcon from '@material-ui/icons/Cancel';
import { withStyles, createStyles, Theme, makeStyles } from "@material-ui/core/styles";

// npm i @date-io/date-fns@1.3.13 (date-fns menggunakna version 1.3.13)
import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import Slider from '@material-ui/core/Slider';
import {createPromo} from '../../controller/PromoController';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import { Promo } from '../../model/Promo';

const iOSBoxShadow = '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';
const IOSSlider = withStyles({
    root: {
        width: '90%',
        marginTop: '5%',
        color: '#3880ff',
        height: 2,
        padding: '15px 0',
    },
    thumb: {
        height: 28,
        width: 28,
        backgroundColor: '#fff',
        boxShadow: iOSBoxShadow,
        marginTop: -14,
        marginLeft: -14,
        '&:focus, &:hover, &$active': {
            boxShadow: '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
            // Reset on touch devices, it doesn't add specificity
            '@media (hover: none)': {
                boxShadow: iOSBoxShadow,
            },
        },
    },
    active: {},
    valueLabel: {
        left: 'calc(-50% + 11px)',
        top: -22,
        '& *': {
            background: 'transparent',
            color: '#fafafa',
        },
    },
    track: {
        height: 2,
    },
    rail: {
        height: 2,
        opacity: 0.5,
        backgroundColor: '#bfbfbf',
    },
    mark: {
        backgroundColor: '#bfbfbf',
        height: 8,
        width: 1,
        marginTop: -3,
    },
    markActive: {
        opacity: 1,
        backgroundColor: 'currentColor',
    },
})(Slider);

const marks = [
    { value: 0 }, { value: 10 }, { value: 20 }, { value: 30 }, { value: 40 }, { value: 50 },
    { value: 60 }, { value: 70 }, { value: 80 }, { value: 90 }, { value: 100 }
];

interface ModalProps {
    open: boolean;
    onClose(): void;

}
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),
            backgroundColor: '#063852',
            color: '#90AFC5'
        },
        formStyle: {
            paddingTop: theme.spacing(2)
        },
        buttonStyle: {
            marginTop: theme.spacing(10),
            alignSelf: 'center',
        },

    })
);
const CreatePromoModal = (props: ModalProps) => {
    const classes = useStyles();
    const [promoCode, setPromoCode] = React.useState('');
    const [promoTitle, setPromoTitle] = React.useState('');
    const [promoDiscount, setPromoDiscount] = React.useState(0);
    const [selectedStartDate, setSelectedStartDate] = React.useState<Date | null>(
        new Date(Date.now())
    );
    const [selectedExpiredDate, setSelectedExpiredDate] = React.useState<Date | null>(
        new Date(Date.now())
    );
    const [loading, setLoading] = React.useState(false);
    const today = new Date();
    const handleStartDate = (date: Date | null) => {
        setSelectedStartDate(date);
    }
    const handleExpiredDate = (date: Date | null) => {
        setSelectedExpiredDate(date);
    }
    const modalStyle = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    };

    const handleDiscount = (e: any, value: any) => {
        setPromoDiscount(value);
    }

    const handleCancel = () => {
        props.onClose();
    }

    const handleAddPromo = async () => {
        if (promoCode === "" || promoTitle === "" || selectedStartDate === null || selectedExpiredDate === null) {
            alert("please fill in all fields!!");
        } else {
            setLoading(true);
            const newPromo: Promo = {
                codePromo: promoCode,
                promoTitle: promoTitle,
                startDate: selectedStartDate.getTime(),
                expiredDate: selectedExpiredDate.getTime(),
                discount: promoDiscount/100,
            }
            const createPomo = await createPromo(newPromo);
            if(createPomo === true){
                setLoading(false);
                window.location.reload();
              }else{
                setLoading(false);
                window.alert(`This Promo is Already`);
              
              }
        }
    }
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <Container maxWidth="sm" className={classes.root}>
                <Typography variant="h4">Create Promo</Typography>
                <form className={classes.formStyle}>
                    <TextField
                        variant='outlined'
                        label='Promo Code'
                        style={{ marginBottom: 20, width: 500 }}
                        value={promoCode}
                        onChange={(ev) => setPromoCode(ev.target.value)}
                    />
                    <TextField
                        variant='outlined'
                        label='Promo Title'
                        style={{ marginBottom: 20, width: 500 }}
                        value={promoTitle}
                        onChange={(ev) => setPromoTitle(ev.target.value)}
                    />
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            format="MM/dd/yyyy"
                            label='Starting Date'
                            minDate={today}
                            value={selectedStartDate}
                            onChange={handleStartDate}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                        <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            format="MM/dd/yyyy"
                            label='Expired Date'
                            minDate={today}
                            value={selectedExpiredDate}
                            onChange={handleExpiredDate}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </MuiPickersUtilsProvider>
                    <Typography gutterBottom>Discount</Typography>
                    <IOSSlider aria-label="ios slider" value={promoDiscount} marks={marks} valueLabelDisplay="auto" onChange={handleDiscount}  />

                    <Button
                        variant='text'
                        color='secondary'
                        startIcon={<CancelIcon />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={handleCancel}
                    >
                        <b>Cancel</b>
                    </Button>
                    <Button
                        variant='text'
                        color='primary'
                        startIcon={<Add />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={handleAddPromo}
                    >
                        <b>ADD Promo</b>
                    </Button>
                </form>
                <FullScreenSpinner open={loading} />
            </Container>
        </Modal>
    )
}



export default CreatePromoModal;