import * as React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';

import CreatePromoModal from './CreatePromoModal';

const EmptyOrderCard = () => {
    const [modalCreate, setModalCreate] = React.useState(false);
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
    };
    
    const createMenu = () => {
        setModalCreate(true);
    }
  
  return (
    <Container style={containerStyle}>
      <Card style={{ maxWidth: 300 }}>
        <CardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            Restaurant Promo
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            Add an Promo for Your Restaurant
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size='small'
            color='primary'
            style={{ fontWeight: 'bold', marginLeft: 'auto' }}
            onClick={createMenu}
          >
            Create
          </Button>
        </CardActions>
      </Card>
      <CreatePromoModal
        open={modalCreate}
        onClose={() => {
            setModalCreate(false);
        }}

      />
    </Container>

  )
};

export default EmptyOrderCard;
