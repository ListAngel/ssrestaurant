import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
    TableContainer,
    Paper,
    Table,
    TableBody,
    TableHead,
    TableRow,
    TableCell,
    Button,
    Container,
    Typography,
    TextField,
    Select,
    MenuItem
} from '@material-ui/core';
import { Promo } from '../../model/Promo';
import { getAllPromo } from '../../controller/PromoController';
import { useHistory } from 'react-router-dom';
import { renderTime, renderDiscount } from '../../utils/RenderUtils';
import { Add } from '@material-ui/icons';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import EmptyPromo from './EmptyPromo';
import CreatePromoModal from './CreatePromoModal';
import DetelePromoModal from './DeletePromoModal';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';

const StyledTableCell = withStyles(() => ({
    head: {
        backgroundColor: 'gray',
        color: 'white',
        fontSize: 20,
    },
}))(TableCell);

const TableHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell style={{ width: 200, textAlign: 'center' }}>
                    Actions
                </StyledTableCell>
                <StyledTableCell>Promo Code</StyledTableCell>
                <StyledTableCell>Promo Title</StyledTableCell>
                <StyledTableCell>Start Date</StyledTableCell>
                <StyledTableCell>Expired Date</StyledTableCell>
                <StyledTableCell>Discount</StyledTableCell>

            </TableRow>
        </TableHead>
    );
};

const useStyles = makeStyles(theme => ({
    root: {
        paddingBottom: theme.spacing(8),
    },
    paper: {
        marginTop: theme.spacing(5),
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select:{
        height: 55,
        marginLeft: theme.spacing(3),

    }
}))

const ShowPromo = () => {
    const defaultPromoSelected: Promo = {
        id: 'null',
        codePromo: 'null',
        promoTitle: 'null',
        startDate: 0,
        expiredDate: 0,
        discount: 0,
    }
    const [promoSelected, setPromoSelected] = React.useState<Promo>(defaultPromoSelected);
    const history = useHistory();
    const classes = useStyles();
    const today = new Date().getTime();
    const [promo, setPromo] = React.useState([] as any[]);
    const [filterPromo, setFilterPromo] = React.useState([] as any[]);
    const [tempFilterPromo, setTempFilterPromo] = React.useState([] as any[]);
    const [loading, setLoading] = React.useState(true);
    const [txtSearch, setTxtSearch] = React.useState('');
    const [status, setStatus] = React.useState('All');
    const [modalCreate, setModalCreate] = React.useState(false);
    const [modalDelete, setModalDelete] = React.useState(false);
    // Get All Promo With Contoller Promo
    React.useEffect(() => {
        getAllPromo().then(result => {
            setPromo(result);
            setTempFilterPromo(result);
            setFilterPromo(result);
            setLoading(false);
        })
    }, []);

    if (promo.length < 1 && loading === false) {
        return (
            <div>
                <EmptyPromo />
            </div>
        );
    }


    const handleUpdatePromo = (item: Promo) => {
        history.push(`/admin/promo/edit/${item.id}`);
    }

    const handleDeletePromo = async (promo: Promo) => {
        setPromoSelected(promo);
        setModalDelete(true);
    }

    const renderTableBody = (items: Promo[]) => {
        const result = items.map((item: Promo, k) => {
            return (
                <TableRow key={k}>
                    <TableCell style={{ textAlign: 'center' }}>
                        <Button color='primary' onClick={() => handleUpdatePromo(item)}>
                            <EditIcon />
                        </Button>{' '}
                             |
                        <Button color='secondary' onClick={() => handleDeletePromo(item)}>
                            <DeleteIcon />
                        </Button>
                    </TableCell>
                    <TableCell>{item.codePromo}</TableCell>
                    <TableCell>{item.promoTitle}</TableCell>
                    <TableCell>{renderTime(item.startDate)}</TableCell>
                    <TableCell>{renderTime(item.expiredDate)}</TableCell>
                    <TableCell>{renderDiscount(item.discount)}</TableCell>

                </TableRow>
            );
        })
        return result;
    }
    const handleSearchPromo = (ev: any) => {
        setTxtSearch(ev.target.value);
        
        const temp = tempFilterPromo.filter((item: any) =>
            item.codePromo.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterPromo(temp);
         
    };

    const handleStatusActive = (ev: any) => {
        setStatus(ev.target.value);

        if(ev.target.value === "All"){
            setFilterPromo(promo);
            setTempFilterPromo(promo);
        }else if(ev.target.value === "Active"){
            const temp = promo.filter((item: any) => {
                return ((item.startDate <= today && item.expiredDate >= today) || (item.startDate >= today && item.expiredDate >= today))
            });
            setFilterPromo(temp);
            setTempFilterPromo(temp);
        }else {
            const temp = promo.filter((item: any) => {
                return  item.expiredDate <= today
            });
            setFilterPromo(temp);
            setTempFilterPromo(temp);
        }  
    }
    const createPromo = () => {
        setModalCreate(true);
    }
    return (
        <Container maxWidth="md" className={classes.root} >
            <Typography variant='h4' component='h4'>
                Promo Management
            </Typography>
            <form>
                <TextField
                    className={classes.search}
                    label='Search Promo'
                    variant='outlined'
                    value={txtSearch}
                    onChange={(ev) => handleSearchPromo(ev)}
                ></TextField>
                <Select
                    className = {classes.select}
                    style={{ marginTop: 20 }}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={status}
                    onChange={handleStatusActive}
                >
                    <MenuItem value={"All"}>All</MenuItem>
                    <MenuItem value={"Active"}>Active</MenuItem>
                    <MenuItem value={"Deactive"}>Deactive</MenuItem>
                </Select>
                <Button
                    variant='contained'
                    startIcon={<Add />}
                    color='primary'
                    style={{ height: 55, marginLeft: 20 }}
                    onClick={createPromo}
                >
                    Add New Promo
                    </Button>
            </form>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table>
                        <TableHeader />
                        <TableBody>{renderTableBody(filterPromo)}</TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <CreatePromoModal
                open={modalCreate}
                onClose={() => {
                    setModalCreate(false);
                }}

            />
            <DetelePromoModal
                open={modalDelete}
                promoDeleted={promoSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}

            />
            <FullScreenSpinner open={loading} />
        </Container>
    )
}




export default ShowPromo;