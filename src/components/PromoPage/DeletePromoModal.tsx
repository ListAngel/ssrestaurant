import React from 'react';
import { Promo } from '../../model/Promo';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import {deletePromo} from '../../controller/PromoController';

interface promoProps {
    open: boolean;
    onClose(open: boolean): void;
    promoDeleted: Promo;
}
const DeletePromoModal = (props: promoProps) => {
    const [loading, setLoading] = React.useState(false);

    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (promoDelete: Promo) => {
        setLoading(true);
        await deletePromo(promoDelete);
        setLoading(false);
        window.location.reload();
    }

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Promo"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.promoDeleted.codePromo} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleDelete(props.promoDeleted)}} color="primary">
                    Yes
                </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
                </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}

export default DeletePromoModal;