import React from 'react';
import { Modal, CardMedia } from '@material-ui/core';

interface imageProps {
    open: boolean;
    onClose(): void;
    image: string;
}

const modalStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
};

const ShowImageModal = (props: imageProps) => {
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <CardMedia
                image={props.image}
                style={{width:'800px',height:'500px'}}
            />
        </Modal>
    )
}


export default ShowImageModal;