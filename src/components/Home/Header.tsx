import React from 'react';
import {Container, Typography, Box } from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      marginTop: theme.spacing(5),
      padding: theme.spacing(2),
      textAlign: 'center',
      color: '#F0810F',
    },
  }),
);


const Header = () => {
    const classes = useStyles();
    return(
        <Container maxWidth="xl" className={classes.paper}>
             <Typography variant="h2">
                <Box fontFamily="Monospace">
                        Menus
                </Box>
            </Typography>
            <Typography variant="h5">
                <Box fontFamily="Monospace">
                        The Best Taste for You
                </Box>
            </Typography>
        </Container>
        
    )
}



export default Header;