import React from 'react';
import {
    Container,
    TableHead,
    TableRow,
    TableCell,
    Button,
    Grid,
    Avatar,
    Typography,
    Box,
    ButtonBase,
    Tooltip,
    Fab,
    Badge,
    IconButton,
} from '@material-ui/core';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { Menu } from '../../model/Menu';
import { MenuOrder } from '../../model/MenuOrder';
import { getAllMenu } from '../../controller/MenuController';
import Meat from '../../assets/svg/Meat';
import SeaFood from '../../assets/svg/Seafood';
import Vegetable from '../../assets/svg/Vegetable';
import { renderCurrency } from '../../utils/RenderUtils';
import Footer from './Footer';
import { createStyles, withStyles, Theme, makeStyles } from "@material-ui/core/styles";
import ShowImageModal from './ShowImageModal';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import CreateOrderModal from '../OrderPage/CreateOrderModal';
import { Tables } from '../../model/Tables';
import { getAllTableAvailable } from '../../controller/TableController';
import {Reservation} from '../../model/Reservation';
import {getReservationWithId} from '../../controller/ReservationController';
import { useParams } from 'react-router-dom';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        buttonStyle: {
            marginTop: theme.spacing(10),
            alignSelf: 'center',
            width: '150px',
            height: '100px',
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: '#F0810F',
        },
        paperListMenu: {
            padding: theme.spacing(2),
            margin: 'auto',
            maxWidth: 500,
        },
        renderBody: {
            marginTop: theme.spacing(10),
        },
        listMenu: {
            marginTop: theme.spacing(2),
            marginBottom: theme.spacing(10),
        },
        absolute: {
            position: 'absolute',
            bottom: theme.spacing(2),
            right: theme.spacing(3),
        },

    })
);
const ListMenu = () => {
    const [listMenu, setListMenu] = React.useState([] as any[]);
    const [filterMenu, setFilterMenu] = React.useState([] as any[]);
    const [filterMenuType, setFilterMenuType] = React.useState("");
    const [imageSelected, setSelectedImage] = React.useState("");
    const [modalImageShow, setModalImageShow] = React.useState(false);
    const [menuSelected, setMenuSelected] = React.useState<MenuOrder[]>([]);
    const [countMenuOrder, setCountMenuOrder] = React.useState(0);
    const [loading, setLoading] = React.useState(true);
    const [modalDetailOrder, setModalDetailOrder] = React.useState(false);
    const [listTable, setListTable] = React.useState<Tables[]>([]);
    const [tableReservation, setTableReservation] = React.useState<Tables>();
    const [reservationSelected, setReservationSelected] = React.useState<Reservation>();
    const classes = useStyles();
    const { idRvn } = useParams();
    React.useEffect(() => {
        getAllMenu().then(result => {
            setFilterMenuType("All")
            const temp = result.filter((item: any) => {
                return item.stock > 0
            });
            setListMenu(temp);
            setFilterMenu(temp);
            setLoading(false);
        })
        console.log("aaa",idRvn);
        if(idRvn !== undefined){
            getReservationWithId(idRvn || 'null').then(result => {
                setReservationSelected(result);
                setTableReservation(result.tables);
            })
            getAllTableAvailable().then(result => {
                setListTable(result);
            })
        }else{
            getAllTableAvailable().then(result => {
                setListTable(result);
            })
        }
        

    }, []);

    const btnMeat = () => {
        const temp = listMenu.filter((item: any) => {
            return item.type === "Meat"
        });
        setFilterMenuType("Meat");
        setFilterMenu(temp);
    }

    const btnSeaFood = () => {
        const temp = listMenu.filter((item: any) => {
            return item.type === "Sea Food"
        });
        setFilterMenuType("Sea Food");
        setFilterMenu(temp);
    }

    const btnVegetable = () => {
        const temp = listMenu.filter((item: any) => {
            return item.type === "Vegetable"
        });
        setFilterMenuType("Vegetable");
        setFilterMenu(temp);
    }

    const renderHeader = () => {
        return (
            <TableHead>
                <TableRow>
                    <TableCell>
                        <Button
                            variant='text'
                            className={classes.buttonStyle}
                            onClick={btnMeat}
                        >
                            <Meat />
                        </Button>
                    </TableCell>
                    <TableCell>
                        <Button
                            variant='text'
                            className={classes.buttonStyle}
                            onClick={btnSeaFood}
                        >
                            <SeaFood />
                        </Button>
                    </TableCell>
                    <TableCell>
                        <Button
                            variant='text'
                            className={classes.buttonStyle}
                            onClick={btnVegetable}
                        >
                            <Vegetable />
                        </Button>
                    </TableCell>
                </TableRow>
            </TableHead>
        );
    }
    const StyledBadge = withStyles((theme: Theme) =>
        createStyles({
            badge: {
                right: -3,
                top: 13,
                border: `2px solid ${theme.palette.background.paper}`,
                padding: '0 4px',
            },
        }),
    )(Badge);

    const handleImageShow = (image: string) => {
        setModalImageShow(true);
        setSelectedImage(image);
    }

    const handleRemoveMenu = (addMenu: Menu) => {
        let check: boolean = false;
        menuSelected.forEach(function (doc, index, object) {
            if (doc.menu.id === addMenu.id) {
                if (doc.quantity === 1) {
                    object.splice(index, 1);
                    check = true;
                } else if (doc.quantity > 1) {
                    doc.quantity = doc.quantity - 1;
                }
            }
        });
        if (check !== false) {
            setCountMenuOrder(countMenuOrder - 1);

        }
    }

    const handleAddMenu = (addMenu: Menu) => {
        let check: boolean = false;
        menuSelected.forEach((doc) => {
            if (doc.menu.id === addMenu.id) {
                if (doc.quantity < addMenu.stock) {
                    doc.quantity = doc.quantity + 1;
                } else {
                    window.alert(`Quantity Over Load`);
                }
                check = true;
            }

        });

        let result = menuSelected;

        if (check === false) {
            let newMenuOrder: MenuOrder = {
                menu: addMenu,
                quantity: 1,
            }
            result.push(newMenuOrder);
        }

        setMenuSelected(result);
        setCountMenuOrder(menuSelected.length);
    }
    const countItem = (item: Menu) => {
        let result: number = 0;
        if (menuSelected.length > 0) {
            menuSelected.forEach(function (doc, index, object) {
                if (doc.menu.id === item.id) {
                    result = doc.quantity;
                }
            })
        }
        return result;
    }
    const renderBody = (filterMenu: Menu[]) => {
        return (
            <div>
                <Grid container spacing={2} className={classes.renderBody}>
                    <Grid item xs={12} className={classes.paper}>
                        <Typography variant="h2">
                            <Box fontFamily="Monospace">
                                {filterMenuType}
                            </Box>
                        </Typography>
                    </Grid>
                </Grid>
                <Grid container spacing={2} className={classes.listMenu}>
                    {filterMenu.map((item) => (
                        <Grid item xs={6}>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <Badge badgeContent={countItem(item)} color="primary">
                                        <ButtonBase
                                            onClick={() => { handleImageShow(item.urlImage) }}
                                        >
                                            <Avatar alt="Remy Sharp" style={{ width: '70px', height: '70px' }} src={item.urlImage} />
                                        </ButtonBase>
                                    </Badge>
                                </Grid>

                                <Grid item xs={12} sm container>
                                    <Grid item xs container direction="column" spacing={2}>
                                        <Grid item xs>
                                            <Typography gutterBottom variant="subtitle1">
                                                {item.name}
                                            </Typography>
                                            <Typography variant="body2" gutterBottom>
                                                {item.type}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item>
                                        <Typography variant="subtitle1">{renderCurrency(item.price)}</Typography>

                                    </Grid>
                                    {listTable.length !== 0 && reservationSelected === undefined ? (
                                        <Grid item>
                                            <Tooltip title="Remove To Order">
                                                <IconButton aria-label="Order"
                                                    onClick={() => { handleRemoveMenu(item) }}
                                                >
                                                    <RemoveIcon />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Add To Order">
                                                <IconButton aria-label="Order"
                                                    onClick={() => { handleAddMenu(item) }}
                                                >
                                                    <AddIcon />
                                                </IconButton>
                                            </Tooltip>
                                        </Grid>
                                    ) : null}
                                     {reservationSelected !== undefined ? (
                                        <Grid item>
                                            <Tooltip title="Remove To Order">
                                                <IconButton aria-label="Order"
                                                    onClick={() => { handleRemoveMenu(item) }}
                                                >
                                                    <RemoveIcon />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Add To Order">
                                                <IconButton aria-label="Order"
                                                    onClick={() => { handleAddMenu(item) }}
                                                >
                                                    <AddIcon />
                                                </IconButton>
                                            </Tooltip>
                                        </Grid>
                                    ) : null}

                                </Grid>
                            </Grid>
                        </Grid>
                    ))}
                </Grid>
                <ShowImageModal
                    open={modalImageShow}
                    image={imageSelected}
                    onClose={() => {
                        setModalImageShow(false);
                    }}
                />
            </div>


        )
    }

    const handleOrder = () => {
        setModalDetailOrder(true)

    }
    const handleCloseModal = () => {
        setModalDetailOrder(false);
    };
    const renderToolTip = () => {
        return (
            <Tooltip title="Add" aria-label="add">
                <Fab color="secondary" className={classes.absolute} onClick={handleOrder}>
                    <StyledBadge badgeContent={countMenuOrder} color="secondary">
                        <ShoppingCartIcon />
                    </StyledBadge>
                </Fab>
            </Tooltip>
        )
    }
    return (
        <div>
            <FullScreenSpinner open={loading} />
            <Container maxWidth="sm">
                {renderHeader()}
            </Container>
            <Container maxWidth="md">
                {renderBody(filterMenu)}
            </Container>
            <Footer />
            {listTable.length !== 0 ? (renderToolTip()):null}
            {reservationSelected !== null ? (renderToolTip()):null}
            {reservationSelected !== null ? (
                <CreateOrderModal
                    open={modalDetailOrder}
                    menuOrder={menuSelected}
                    listTable={listTable}
                    orderReservation={reservationSelected}
                    onClose={() => {
                        handleCloseModal();
                    }}
                />
            ):null}
            
        </div>

    )
}

export default ListMenu;