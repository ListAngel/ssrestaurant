import React from 'react';
import {
    Container,
    Grid,
    Typography,
    Box,
    TableHead,
    TableRow,
    TableCell,
} from '@material-ui/core';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        title: {
            marginBottom: theme.spacing(2),
        },
        icons: {
            marginRight: theme.spacing(2),
            marginTop: theme.spacing(2),
        }
    }),
);
const Footer = () => {
    const classes = useStyles();
    return (
        <Container maxWidth="xl" style={{ backgroundColor: '#011A27', color: '#FCFDFE' }}>
            <Container maxWidth="lg">
                <Grid container spacing={3}>
                    <Grid item xs={6}>
                        <Typography variant="h4">
                            <Box fontFamily="Arial" className={classes.title}>
                                Contact information
                            </Box>
                        </Typography>
                        <Typography variant="h6">
                            <Box fontFamily="Monospace">
                                ADDRESS: 100 Tenth Avenue, New York City, NY 1001
                            </Box>
                            <Box fontFamily="Monospace">
                                FOR BOOKING: (044) 359 0173
                            </Box>
                            <Box fontFamily="Monospace">
                                FOLLOW US ON:
                            </Box>
                            <Box fontFamily="Monospace">
                                <FacebookIcon className={classes.icons} />
                                <InstagramIcon className={classes.icons} />
                                <TwitterIcon className={classes.icons} />
                            </Box>


                        </Typography>
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant="h4">
                            <Box fontFamily="Arial" className={classes.title}>
                                Restaurant hours
                            </Box>

                        </Typography>
                        <Typography variant="h6">
                            <Box fontFamily="Monospace">
                                LUNCH:
                            </Box>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Monday - Friday</TableCell>
                                    <TableCell>11:30AM - 2:00PM</TableCell>
                                </TableRow>
                            </TableHead>
                        </Typography>
                        <Typography variant="h6">
                            <Box fontFamily="Monospace">
                                DINNER:
                            </Box>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Monday - Friday</TableCell>
                                    <TableCell>5:30PM - 11:00PM</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>Saturday - Sunday</TableCell>
                                    <TableCell>4:30PM - 10:00PM</TableCell>
                                </TableRow>
                            </TableHead>
                        </Typography>
                    </Grid>
                </Grid>
            </Container>
        </Container>

    )
}


export default Footer;