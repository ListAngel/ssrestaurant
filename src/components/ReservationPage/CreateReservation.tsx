import React from 'react';
import {
    Container,
    Typography,
    TextField,
    Button,
    Select,
    MenuItem,
    Grid,
} from "@material-ui/core";
import {
    KeyboardDatePicker,
    KeyboardTimePicker,
    MuiPickersUtilsProvider,
  } from '@material-ui/pickers';
import { Add } from '@material-ui/icons';
import CancelIcon from '@material-ui/icons/Cancel';
import { Theme, makeStyles } from "@material-ui/core/styles";
import { Tables } from '../../model/Tables';
import { getAllTable } from '../../controller/TableController';
import { Reservation } from '../../model/Reservation';
import {createReservation} from '../../controller/ReservationController';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import { useHistory, useParams } from 'react-router-dom';
import DateFnsUtils from '@date-io/date-fns';
const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: '100%',
        width: '75%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'start',
        alignItems: 'stretch',
        marginTop: theme.spacing(5),
    },
    form: {
        width: '80%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        // flexWrap: 'wrap',
        padding: '16px 0',
    },
    buttonStyle: {
        width: '30%',
        backgroundColor: '#0063B2FF',
        color: '#9CC3D5FF',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    buttonEnd: {
        alignSelf: 'center',
        marginTop: theme.spacing(3),
    },
    select: {
        width: '100%',
        marginBottom: theme.spacing(2),

    }
}));

const CreateReservation = () => {
    const classes = useStyles();
    const history = useHistory();
    const defaultTable: Tables = {
        id: 'null',
        tableNumber: "",
        type: "",
        capacity: 0,
        status: "",
    }
    const today = new Date();
    const [tableSelected, setTableSelected] = React.useState<Tables>(defaultTable);
    const [loading, setLoading] = React.useState(false);
    const [tableId, setTableId] = React.useState("");
    const [picName, setPicName] = React.useState('');
    const [reservationDate, setReservationDate] = React.useState<Date | null>(new Date());
    
    const [listTable, setListTable] = React.useState([] as any[]);

    React.useEffect(() => {
        getAllTable().then(result => {
            setListTable(result);
        })
        setLoading(false);
    }, []);


    const handleTableSelected = (ev: any) => {
        setTableId(ev.target.value);
        listTable.forEach((doc) => {
            if (doc.id === ev.target.value) {
                setTableSelected(doc);
            }
        })
    }
    const renderListTable = (listTable: Tables[]) => {
        return (
            <Select
                className={classes.select}
                style={{ marginTop: 20 }}
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={tableId}
                onChange={handleTableSelected}
            >
                {listTable.map((item) => (
                    <MenuItem key={item.tableNumber} value={item.id}>
                        {item.tableNumber}
                    </MenuItem>
                ))}

            </Select>
        )
    }
    const modalStyle = {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    };
    const btnGroupStyle: React.CSSProperties = {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
    };

    const handleDateChange = (date: Date | null) => {
        setReservationDate(date);
    };

    const handleCreateReservation = async () => {
        setLoading(true);
        if(reservationDate !== null){
            const newReservation: Reservation = {
                pic: picName,
                tables: tableSelected,
                reservationDate: reservationDate.getTime(),
                reservationCreated: today.getTime(),
                status: "On Process"
            }
            const checker = await createReservation(newReservation);
            if(checker === true){
                setLoading(false);
                history.push('/reservation');
            }else{
                setLoading(false);
                window.alert(`This Reservation is Already`);
              
            }
        }else {
            alert("Date is Invalid!!");
        }
       
    }
   

    return (
        <Container className={classes.root}>
            <div style={{ flexGrow: 1 }}>
                <Typography variant='h4'>
                    Create Reservation
                    </Typography>
            </div>
            <form className={classes.form}>
                <TextField
                    variant='outlined'
                    label='PIC Name'
                    style={{ marginBottom: 20, width: '100%' }}
                    value={picName}
                    onChange={(ev) => setPicName(ev.target.value)}
                />
                {renderListTable(listTable)}
                {tableId !== "" ? (
                    <Typography>
                        Capacity: {tableSelected?.capacity}
                    </Typography>
                ) : null}
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid container justify='space-between'>
                        <KeyboardDatePicker
                            label='Pick Date'
                            format='dd MMMM yyyy'
                            minDate={today.setDate(today.getDate() + 1)}
                            style={{ flexGrow: 1, margin: 16 }}
                            value={reservationDate}
                            onChange={handleDateChange}
                        />
                        <KeyboardTimePicker
                            label='Pick Time'
                            style={{ flexGrow: 1, margin: 16 }}
                            value={reservationDate}
                            onChange={handleDateChange}
                        />
                    </Grid>
                </MuiPickersUtilsProvider>
                <div style={btnGroupStyle}>
                    <Button
                        variant='text'
                        color='secondary'
                        startIcon={<CancelIcon />}
                        disableRipple
                        className={classes.buttonEnd}
                        onClick={() => {
                            history.push('/reservation');
                        }}
                    >
                        <b>Cancel</b>
                    </Button>
                    <Button
                        variant='text'
                        color='primary'
                        startIcon={<Add />}
                        disableRipple
                        className={classes.buttonEnd}
                        disabled={
                            tableId.length === 0 ||
                            picName.length < 3
                        }
                        onClick={handleCreateReservation}
                    >
                        <b>Create Reservation</b>
                    </Button>
                </div>
            </form>
            <FullScreenSpinner open={loading} />
        </Container>
    )
}

export default CreateReservation;