import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import {
    TableContainer,
    Paper,
    Table,
    TableBody,
    TableHead,
    TableRow,
    TableCell,
    Button,
    Container,
    Typography,
    TextField,
    Select,
    MenuItem,
    Tooltip,
} from '@material-ui/core';
import { renderTime } from '../../utils/RenderUtils';
import CachedIcon from '@material-ui/icons/Cached';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';
import ClearIcon from '@material-ui/icons/Clear';
import EmptyReservation from './EmptyReservation';
import DeleteReservation from './DeleteReservation';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import { Reservation } from '../../model/Reservation';
import { getAllReservation } from '../../controller/ReservationController';
import { Tables } from '../../model/Tables';

// import DeleteTablesModal from './DeleteTablesModal';

const useStyles = makeStyles(theme => ({
    root: {
        paddingBottom: theme.spacing(8),
    },
    paper: {
        marginTop: theme.spacing(5),
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select: {
        height: 55,
        marginLeft: theme.spacing(3),

    },
    done: {
        color: '#64dd17',
        marginLeft: theme.spacing(2),

    },
    cancel: {
        color: '#d50000',
        marginLeft: theme.spacing(2),
    }
}))
const StyledTableCell = withStyles(() => ({
    head: {
        backgroundColor: 'gray',
        color: 'white',
        fontSize: 20,
    },
}))(TableCell);

const TableHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell style={{ width: 200, textAlign: 'center' }}>
                    Actions
                </StyledTableCell>
                <StyledTableCell>PIC Name</StyledTableCell>
                <StyledTableCell>Created Reservation</StyledTableCell>
                <StyledTableCell>Reservation Date</StyledTableCell>
                <StyledTableCell>Table Number</StyledTableCell>
                <StyledTableCell>Status</StyledTableCell>
            </TableRow>
        </TableHead>
    );
};
const ShowReservationPage = () => {
    const history = useHistory();
    const classes = useStyles();
    const [txtSearch, setTxtSearch] = React.useState('');
    const [reservation, setReservation] = React.useState([] as any[]);
    const [modalCreate, setModalCreate] = React.useState(false);
    const [modalDelete, setModalDelete] = React.useState(false);
    const [loading, setLoading] = React.useState(true);
    const [status, setStatus] = React.useState('All');
    const [filterReservation, setFilterReservation] = React.useState([] as any[]);
    const [tempFilterReservation, setTempFilterReservation] = React.useState([] as any[]);

    const defaultTableSelected: Tables = {
        id: '',
        tableNumber: '',
        type: '',
        capacity: 0,
        status: '',
    }

    const defaultReservation: Reservation = {
        id: "",
        pic: "",
        tables: defaultTableSelected,
        reservationCreated: 0,
        reservationDate: 0,
        status: "",
    }
    const [reservationSelected, setReservationSelected] = React.useState<Reservation>(defaultReservation);

    React.useEffect(() => {
        getAllReservation().then((result) => {
            console.log(result);
            setReservation(result);
            setFilterReservation(result);
            setTempFilterReservation(result);
            setLoading(false);
        })
    }, []);

    if (reservation.length < 1 && loading === false) {
        return (
            <div>
                <EmptyReservation />
            </div>
        );
    }

    const handleDeleteReservvation = async (reservation: Reservation) => {
        setReservationSelected(reservation);
        setModalDelete(true);
    }

    const handleOrderReservation = (idReservation: string) => {
        console.log("AA");
        history.push('/' + idReservation);
    }

    const renderTableBody = (items: Reservation[]) => {
        const result = items.map((item: Reservation, k) => {
            return (
                <TableRow key={k}>
                    {item.status === 'On Process' ? (
                        <TableCell style={{ textAlign: 'center' }}>
                            <Button color='primary' onClick={() => handleUpdateReservation(item)}>
                                <EditIcon />
                            </Button>{' '}
                                    |
                            <Button color='secondary' onClick={() => handleDeleteReservvation(item)}>
                                <DeleteIcon />
                            </Button>
                        </TableCell>
                    ) : (
                            <TableCell style={{ textAlign: 'center' }}>
                                <ClearIcon />
                            </TableCell>
                        )}
                    <TableCell>{item.pic}</TableCell>
                    <TableCell>{renderTime(item.reservationCreated)}</TableCell>
                    <TableCell>{renderTime(item.reservationDate)}</TableCell>
                    <TableCell>{item.tables.tableNumber}</TableCell>
                    {item.status === 'On Process' ? (
                        <TableCell>
                            <Tooltip title='Order For Your Reservation' arrow>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    startIcon={<CachedIcon />}
                                    onClick={() => {
                                        handleOrderReservation(String(item.id));
                                    }}
                                >
                                    {item.status}
                                </Button>
                            </Tooltip>
                        </TableCell>


                    ) : null}
                    {item.status === 'Done' ? (
                        <div className={classes.done}>
                            <CheckCircleOutlineIcon />
                            <Typography>
                                Order
                            </Typography>
                        </div>
                    ) : null}
                    {item.status === 'Reserved Order' ? (
                        <div className={classes.done}>
                            <CheckCircleOutlineIcon />
                            <Typography>
                                Order Done
                            </Typography>
                        </div>
                    ) : null}
                     {item.status === 'Cancel' ? (
                        <div className={classes.cancel}>
                            <ClearIcon />
                            <Typography>
                                Canceled
                            </Typography>
                        </div>
                    ) : null}


                </TableRow>
            );

        });
        return result
    }

    const createReservation = () => {
        history.push(`/reservation/create`);
    }

    const handleUpdateReservation = (item: Reservation) => {
        history.push(`/reservation/edit/${item.id}`);
    }
    const handleSearchReservation = (ev: any) => {
        setTxtSearch(ev.target.value);

        const temp = tempFilterReservation.filter((item: any) =>
            item.pic.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterReservation(temp);

    };
    const handleStatusActive = (ev: any) => {
        setStatus(ev.target.value);
        if (ev.target.value === "All") {
            setFilterReservation(reservation);
            setTempFilterReservation(reservation);
        } else if (ev.target.value === "On Process") {
            const temp = reservation.filter((item: any) => {
                return item.status === "On Process"
            });
            setFilterReservation(temp);
            setTempFilterReservation(temp);
        } else if (ev.target.value === "Done") {
            const temp = reservation.filter((item: any) => {
                return item.status === "Done"
            });
            setFilterReservation(temp);
            setTempFilterReservation(temp);
        } else {
            const temp = reservation.filter((item: any) => {
                return item.status === "Canceled"
            });
            setFilterReservation(temp);
            setTempFilterReservation(temp);
        }
    }
    return (
        <Container maxWidth="lg" className={classes.root} >
            <Typography variant='h4' component='h4'>
                Reservation Management
            </Typography>
            <form>
                <TextField
                    className={classes.search}
                    label='Search Table'
                    variant='outlined'
                    value={txtSearch}
                    onChange={(ev) => handleSearchReservation(ev)}
                ></TextField>
                <Select
                    className={classes.select}
                    style={{ marginTop: 20 }}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={status}
                    onChange={handleStatusActive}
                >
                    <MenuItem value={"All"}>All</MenuItem>
                    <MenuItem value={"On Process"}>On Process</MenuItem>
                    <MenuItem value={"Done"}>Done</MenuItem>
                    <MenuItem value={"Canceled"}>Canceled</MenuItem>
                </Select>
                <Button
                    variant='contained'
                    startIcon={<Add />}
                    color='primary'
                    style={{ height: 55, marginLeft: 20 }}
                    onClick={createReservation}
                >
                    Add New Reservation
                </Button>
            </form>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table>
                        <TableHeader />
                        <TableBody>{renderTableBody(filterReservation)}</TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <DeleteReservation
                open={modalDelete}
                reservationDeleted={reservationSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}

            />
            <FullScreenSpinner open={loading} />
        </Container>
    )
}

export default ShowReservationPage;