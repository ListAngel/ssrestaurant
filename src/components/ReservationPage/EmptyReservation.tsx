import React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import CreateReservationModal from './CreateReservation';

const EmptyReservation = () => {
    const [modalCreate, setModalCreate] = React.useState(false);
    const history = useHistory();
    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
    };
    const createTables = () => {
      history.push(`/reservation/create`);
    }
    return (
        <Container style={containerStyle}>
          <Card style={{ maxWidth: 300 }}>
            <CardContent>
              <Typography gutterBottom variant='h5' component='h2'>
                Restaurant Reservation
              </Typography>
              <Typography variant='body2' color='textSecondary' component='p'>
                Add Reservation for Eat at The Restaurant
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                size='small'
                color='primary'
                style={{ fontWeight: 'bold', marginLeft: 'auto' }}
                onClick={createTables}
              >
                Create
              </Button>
            </CardActions>
          </Card>
        </Container>
    )
}

export default EmptyReservation;