import React from 'react';
import { Reservation } from '../../model/Reservation';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import {deleteReservation} from '../../controller/ReservationController';

interface modalProps {
    open: boolean;
    onClose(open: boolean): void;
    reservationDeleted: Reservation;
}
const DeleteReservation = (props: modalProps) =>{
    const [loading, setLoading] = React.useState(false);

    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (tableDeleted: Reservation) => {
        setLoading(true);
        await deleteReservation(tableDeleted);
        setLoading(false);
        window.location.reload();
    }

    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Reservation"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.reservationDeleted.id} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleDelete(props.reservationDeleted)}} color="primary">
                    Yes
                </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
                </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}


export default DeleteReservation;