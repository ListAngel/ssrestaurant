import * as React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';

import CreteMenuModal from './CreateMenuModal';
const EmptyOrderCard = () => {
  const containerStyle = {
    display: 'flex',
    justifyContent: 'center',
  };
  const createMenu = () => {
    setModalCreate(true);
  }
  const [modalCreate, setModalCreate] = React.useState(false);
  return (
    <Container style={containerStyle}>
      <Card style={{ maxWidth: 300 }}>
        <CardContent>
          <Typography gutterBottom variant='h5' component='h2'>
            Restaurant Menu
          </Typography>
          <Typography variant='body2' color='textSecondary' component='p'>
            Add an Menu for Your Restaurant
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size='small'
            color='primary'
            style={{ fontWeight: 'bold', marginLeft: 'auto' }}
            onClick={createMenu}
          >
            Create
          </Button>
        </CardActions>
      </Card>
      <CreteMenuModal
        open={modalCreate}
        onClose={createMenu}

      />
    </Container>

  )
};

export default EmptyOrderCard;
