import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle
} from '@material-ui/core';
import { Menu } from '../../model/Menu';
import {deleteMenu} from '../../controller/MenuController';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';

interface menuProps {
    open: boolean;
    onClose(open:boolean): void;
    menuDelete: Menu;
}
const DeleteMenuModal = (props: menuProps) => {
    const [loading, setLoading] = React.useState(false);
    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (menuDelete: Menu) => {
        
        setLoading(true);
        await deleteMenu(menuDelete);
        
        setLoading(false);
        window.location.reload();
    }
    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Menu"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.menuDelete.name} ?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleDelete(props.menuDelete)}} color="primary">
                    Yes
          </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
          </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}

export default DeleteMenuModal;