import React from 'react';
import {
    Container,
    Grid,
    Card,
    CardMedia,
    CardContent,
    CardActions,
    Typography,
    Button,
    TextField,
    Select,
    MenuItem,
} from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import FullScreenSpinner from "../../components/Loading/FullScreenSpinner";
import { makeStyles } from "@material-ui/core/styles";
import { getAllMenu } from "../../controller/MenuController";
import { Menu } from "../../model/Menu";
import EmptyOrderCard from './EmptyMenu';
import CreteMenuModal from './CreateMenuModal';
import DeleteMenuModal from './DeleteMenuModal';
import { useHistory } from 'react-router-dom';
import NumberFormat from 'react-number-format';

const useStyles = makeStyles(theme => ({
    buttonCreate: {
        marginBottom: theme.spacing(5)
    },
    grid:{
        marginTop: theme.spacing(5)
    },
    cardGrid: {
        paddingBottom: theme.spacing(8)
    },
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column"
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select:{
        height: 55,
        marginLeft: theme.spacing(3),

    },
    cardMedia: {
        paddingTop: "56.25%" // 16:9
    },
    cardContent: {
        flexGrow: 1
    }
}));

const ShowCardMenuList = () => {
    const defaultMenuSelected: Menu = {
        id: 'null',
        name: 'null',
        price: 0,
        type: 'null',
        imageName: 'null',
        urlImage: 'null',
        stock: 0,
    }
    const classes = useStyles();
    const [menu, setMenu] = React.useState([] as any[]);
    const [filterMenu, setFilterMenu] = React.useState([] as any[]);
    const [tempFilterMenu, setTempFilterMenu] = React.useState([] as any[]);
    const [menuSelected, setMenuSelected] = React.useState<Menu>(defaultMenuSelected);
    const [modalCreate, setModalCreate] = React.useState(false);
    const [modalDelete, setModalDelete] = React.useState(false);
    const [loading, setLoading] = React.useState(true);
    const history = useHistory();
    const [txtSearch, setTxtSearch] = React.useState('');
    const [typeFilter, setTypeFilter] = React.useState('All');

    React.useEffect(() => {
        getAllMenu().then(result => {
            setMenu(result);
            setFilterMenu(result);
            setLoading(false);
        })

    }, []);

    if (menu.length < 1 && loading === false) {
        return (
            <div>
                <EmptyOrderCard />
            </div>
        );
    }

    const createMenu = () => {
        setModalCreate(true);
    }

    const handleDeleteMenu = async (menu: Menu) => {
        setMenuSelected(menu);
        console.log("MenuNAMEE", menuSelected);
        setModalDelete(true);
    }

    const handleEditMenu = (menu: Menu) => {
        console.log(menu.id);
        history.push(`/admin/menu/edit/${menu.id}`);
    }
    const handleSearchPromo = (ev: any) => {
        setTxtSearch(ev.target.value);
        const temp = tempFilterMenu.filter((item: any) =>
            item.name.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterMenu(temp);
    };
    const handleTypeFilter = (ev: any) => {
        setTypeFilter(ev.target.value);
        if(ev.target.value === "All"){
            setFilterMenu(menu);
            setTempFilterMenu(menu);
        }else if(ev.target.value === "Sea Food"){
            const temp = menu.filter((item: any) => {
                return item.type === "Sea Food"
            });
            setFilterMenu(temp);
            setTempFilterMenu(temp);
        }else if(ev.target.value === "Vegetable"){
            const temp = menu.filter((item: any) => {
                return item.type === "Vegetable"
            });
            setFilterMenu(temp);
            setTempFilterMenu(temp);
        }else {
            const temp = menu.filter((item: any) => {
                return item.type === "Meat"
            });
            setFilterMenu(temp);
            setTempFilterMenu(temp);
        }  
    }
    
    return (
        <div>
            <FullScreenSpinner open={loading} />
            <Container className={classes.cardGrid} maxWidth="md">
                <Typography variant='h4' component='h4'>
                    Menu Management
                </Typography>
                <form>
                    <TextField
                        className={classes.search}
                        label='Search Menu'
                        variant='outlined'
                        value={txtSearch}
                        onChange={(ev) => handleSearchPromo(ev)}
                    ></TextField>
                    <Select
                        className={classes.select}
                        style={{ marginTop: 20 }}
                        labelId="demo-simple-select-helper-label"
                        id="demo-simple-select-helper"
                        value={typeFilter}
                        onChange={handleTypeFilter}
                    >
                        <MenuItem value={"All"}>All</MenuItem>
                        <MenuItem value={"Meat"}>Meat</MenuItem>
                        <MenuItem value={"Vegetable"}>Vegetable</MenuItem>
                        <MenuItem value={"Sea Food"}>Sea Food</MenuItem>
                    </Select>
                    <Button
                        variant='contained'
                        startIcon={<Add />}
                        color='primary'
                        style={{ height: 55, marginLeft: 20 }}
                        onClick={createMenu}
                    >
                        Add New Promo
                    </Button>
                </form>
                {/* End hero unit */}
                <Grid container spacing={5} className={classes.grid}>
                    {filterMenu.map((item, key) => (
                        <Grid item key={key} xs={12} sm={6} md={4}>
                            <Card className={classes.card}>
                                <CardMedia
                                    className={classes.cardMedia}
                                    image={item.urlImage}
                                    title={item.imageName}
                                />
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {item.name}
                                    </Typography>
                                    <Typography gutterBottom variant="h6" component="h6">
                                        {item.type}
                                    </Typography>
                                   
                                    <NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} renderText={value => <div>{value}</div>} />
                                    <Typography>
                                        Stock: {item.stock}
                                    </Typography>

                                </CardContent>
                                <CardActions>
                                    <Button size="small" color="primary" onClick={() => { handleEditMenu(item) }}>
                                        Edit
                                    </Button>
                                    <Button size="small" color="primary" onClick={() => { handleDeleteMenu(item) }}>
                                        Delete
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
            <CreteMenuModal
                open={modalCreate}
                onClose={() => {
                    setModalCreate(false);
                }}

            />
            <DeleteMenuModal
                open={modalDelete}
                menuDelete={menuSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}

            />
        </div>
    );

}

export default ShowCardMenuList;