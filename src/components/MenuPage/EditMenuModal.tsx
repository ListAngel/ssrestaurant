import React from 'react';
import { Menu } from '../../model/Menu';
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import { Container, Typography, Select, TextField, MenuItem, Button, InputLabel } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";
import NumberFormat from "react-number-format";
import UpdateIcon from '@material-ui/icons/Update';
import { useHistory, useParams } from 'react-router-dom';
import { getMenuById, updateMenu } from "../../controller/MenuController";
import CancelIcon from '@material-ui/icons/Cancel';

const editMenuStyle = makeStyles(theme => ({
    root: {
        margin: theme.spacing(3),
    },
    formStyle: {
        paddingTop: theme.spacing(5)
    },
    inputLabel: {
        marginTop: theme.spacing(5),
    },
    buttonStyle: {
        marginTop: theme.spacing(15),
        alignSelf: 'center',
    }
}));

interface NumberFormatCustomProps {
    inputRef: (instance: NumberFormat | null) => void;
    onChange: (event: { target: { name: string; value: string } }) => void;
    name: string;
}


function NumberFormatCustom(props: NumberFormatCustomProps) {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        name: props.name,
                        value: values.value
                    }
                });
            }}
            thousandSeparator
            isNumericString
            prefix="Rp. "
        />
    );
}

const EditMenuModal = () => {
    const classes = editMenuStyle();
    const [menuName, setMenuName] = React.useState("");
    const [menuType, setMenuType] = React.useState("");
    const [value, setValues] = React.useState("0");
    const [image, setImage] = React.useState(null);
    const [menuImageName, setMenuImageName] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const { id } = useParams();
    const [idMenu, setIdMenu] = React.useState("");
    const history = useHistory();
    
    const fetchMenu = async (menuId: string) => {
        await getMenuById(menuId).then((doc) => {
            setIdMenu(menuId);
            setMenuName(doc.name);
            setMenuType(doc.type);
            setValues(String(doc.price));
            setMenuImageName(doc.imageName);

        })
    }

    React.useEffect(() => {
        fetchMenu(id || 'null');
        // eslint-disable-next-line
    }, []);



    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValues(event.target.value);
    };

    const handleSelectChange = (ev: any) => {
        setMenuType(ev.target.value);

    };

    const fileSelectedHandle = (event: any) => {
        if (event.target.files[0]) {
            const image = event.target.files[0];
            setImage(image)
            setMenuImageName(image.name);
        }
    }
    const handleCancel = () => {
        history.push('/admindashboard');
    }
    const updateMenuHandle = async () => {
        setLoading(true);
        let newMenu: Menu = {
            name: menuName,
            type: menuType,
            price: Number(value),
            imageName: menuImageName,
            urlImage: "",
            stock: 0,
        }
        const updateCheck = await updateMenu(idMenu, image, newMenu);
        if(updateCheck === true){
            setLoading(false);
            history.push('/admindashboard');
        }else {
            setLoading(false);
            window.alert(`This Menu is Already`);
        }
      
    }
    return (
        <Container maxWidth="sm">
            <div className={classes.root}>
                <Typography variant='h4' component='h4'>
                    Edit Menu
                </Typography>
                <form className={classes.formStyle}>
                    <TextField
                        margin="normal"
                        fullWidth
                        id="standard-basic"
                        label="Name"
                        value={menuName}
                        onChange={ev => setMenuName(ev.target.value)}
                    />
                    <InputLabel id="demo-simple-select-outlined-label" className={classes.inputLabel}>Menu Type</InputLabel>
                    <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        fullWidth
                        value={menuType}
                        onChange={handleSelectChange}
                        label="Menu Type"
                    >
                        <MenuItem value={"Meat"}>Meat</MenuItem>
                        <MenuItem value={"Vegetable"}>Vegetable</MenuItem>
                        <MenuItem value={"Sea Food"}>Sea Food</MenuItem>
                    </Select>

                    <TextField
                        style={{ marginTop: 20 }}
                        label="Menu Price"
                        fullWidth
                        value={value}
                        onChange={handleChange}
                        name="numberformat"
                        id="formatted-numberformat-input"
                        InputProps={{
                            inputComponent: NumberFormatCustom as any
                        }}
                    />
                    <input type="file" onChange={fileSelectedHandle} accept="image/*" />
                    <Button
                        variant='text'
                        color='secondary'
                        startIcon={<CancelIcon />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={handleCancel}
                    >

                        <b>Cancel</b>
                    </Button>
                    <Button
                        variant='text'
                        color='primary'
                        startIcon={<UpdateIcon />}
                        disableRipple
                        className={classes.buttonStyle}
                        onClick={updateMenuHandle}
                    >

                        <b>UPDATE MENU</b>
                    </Button>
                </form>
            </div>
            <FullScreenSpinner open={loading} />
        </Container>
    )

}

export default EditMenuModal;