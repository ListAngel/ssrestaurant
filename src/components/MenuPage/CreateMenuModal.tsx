import React from "react";
import {
  Container,
  Typography,
  TextField,
  Select,
  MenuItem,
  Button,
  Modal,
} from "@material-ui/core";
import FullScreenSpinner from '../Loading/FullScreenSpinner';
import { Add } from '@material-ui/icons';
import NumberFormat from "react-number-format";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { Menu } from "../../model/Menu";
import { createMenuItem } from "../../controller/MenuController";
import CancelIcon from '@material-ui/icons/Cancel';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing(3),
      backgroundColor: '#063852',
      color: '#90AFC5'
    },
    formStyle: {
      paddingTop: theme.spacing(2)
    },
    buttonStyle: {
      marginTop: theme.spacing(20),
      alignSelf: 'center',
    },

  })
);
interface ModalProps {
  open: boolean;
  onClose(): void;

}
interface NumberFormatCustomProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

function NumberFormatCustom(props: NumberFormatCustomProps) {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={values => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      prefix="Rp. "
    />
  );
}
const CreateMenuModal = (props: ModalProps) => {
  const [value, setValues] = React.useState("");
  const [name, setName] = React.useState("");
  const [type, setType] = React.useState("");
  const [image, setImage] = React.useState(null);
  const [imageName, setImageName] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const classes = useStyles();

  const modalStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

  };
  const fileSelectedHandle = (event: any) => {
    if (event.target.files[0]) {
      const image = event.target.files[0];
      setImage(image)
      setImageName(image.name);
    }
  }

  const addMenu = async () => {
    if (name === "" || type === "" || value === "" || value === "0" || image === null) {
      alert("please fill in all fields!!");
    } else {
      setLoading(true);
      const newMenu: Menu = {
        name: name,
        price: Number(value),
        type: type,
        imageName: imageName,
        urlImage: "",
        stock: 0,
      }

      const createMenu = await createMenuItem(newMenu, image);
      if(createMenu === true){
        setLoading(false);
        window.location.reload();
      }else{
        setLoading(false);
        window.alert(`This Menu is Already`);
        
        // window.location.reload();
      }
      

    }

  }
  const handleCancel = () => {
    props.onClose();
  }
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValues(event.target.value);
  };
  const handleSelectChange = (ev: any) => {
    setType(ev.target.value);

  };
  return (
    <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
      <Container maxWidth="sm" className={classes.root}>
        <Typography variant="h4">Create Menu</Typography>

        <form className={classes.formStyle}>
          <TextField
            style={{ marginTop: 20 }}
            label="Menu Name"
            fullWidth
            required
            value={name}
            onChange={ev => setName(ev.target.value)}
          />

          <Select
            style={{ marginTop: 20 }}
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            fullWidth
            required
            value={type}
            onChange={handleSelectChange}
          >
            <MenuItem value={"Meat"}>Meat</MenuItem>
            <MenuItem value={"Vegetable"}>Vegetable</MenuItem>
            <MenuItem value={"Sea Food"}>Sea Food</MenuItem>
          </Select>

          <TextField
            style={{ marginTop: 20 }}
            label="Menu Price"
            fullWidth
            value={value}
            onChange={handleChange}
            name="numberformat"
            id="formatted-numberformat-input"
            InputProps={{
              inputComponent: NumberFormatCustom as any
            }}
          />
          <input type="file" onChange={fileSelectedHandle} accept="image/*" />
          <Button
            variant='text'
            color='secondary'
            startIcon={<CancelIcon />}
            disableRipple
            className={classes.buttonStyle}
            onClick={handleCancel}
          >
            <b>Cancel</b>
          </Button>
          <Button
            variant='text'
            color='primary'
            startIcon={<Add />}
            disableRipple
            className={classes.buttonStyle}
            onClick={addMenu}
          >
            <b>ADD MENU</b>
          </Button>
        </form>
        <FullScreenSpinner open={loading} />
      </Container>
    </Modal>

  );
};

export default CreateMenuModal;
