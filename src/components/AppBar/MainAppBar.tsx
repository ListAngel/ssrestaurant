import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import MenuAppBarDrawer from './MenuAppBarDrawer';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        grow: {
            flexGrow: 1
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
    }),
);



export default function MainAppBar() {
    const classes = useStyles();
    const [drawerOpen, setDrawerOpen] = React.useState(false);
    return (
        <div className={classes.grow}>
            <AppBar position="relative" style={{backgroundColor:'#011A27'}}>
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="Home"
                        onClick={() => setDrawerOpen(true)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography className={classes.title} variant="h5" >
                        SS-Restaurant
                    </Typography>
                </Toolbar>
                
            </AppBar>
            <MenuAppBarDrawer open={drawerOpen} onClose={() => setDrawerOpen(false)} />

        </div>
    );
}

