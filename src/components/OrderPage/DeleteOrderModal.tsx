import React from 'react'
import { Order } from '../../model/Order';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from '@material-ui/core';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import { deleteOrder } from '../../controller/OrderController';

interface orderProps {
    open: boolean;
    onClose(open: boolean): void;
    orderDeleted: Order;
}

const DeleteOrderModal = (props: orderProps) => {
    const [loading, setLoading] = React.useState(false);

    const handleClose = () => {
        props.onClose(false);
    };

    const handleDelete = async (orderDelete: Order) => {
        setLoading(true);
        const check = await deleteOrder(orderDelete);
        if (check === true) {
            setLoading(false);
            window.location.reload();
        } else {
            window.alert(`This Purchasing can't deleted`);
        }
    }
    return (
        <Dialog open={props.open} onClose={props.onClose}>
            <DialogTitle id="alert-dialog-title">{"Delete Purchasing"} </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    do you want Delete {props.orderDeleted.id} ?
            </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => { handleDelete(props.orderDeleted) }} color="primary">
                    Yes
            </Button>
                <Button onClick={handleClose} color="primary" autoFocus>
                    No
            </Button>
            </DialogActions>
            <FullScreenSpinner open={loading} />
        </Dialog>
    )
}


export default DeleteOrderModal;