import React from 'react';
import { MenuOrder } from '../../model/MenuOrder';
import {
    TableRow,
    TableCell,
    TextField,
    IconButton,
    Typography,
    TableContainer,
    Paper,
    Table,
    TableHead,
    TableBody,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { renderCurrency } from '../../utils/RenderUtils';

interface TableProps {
    menuOrder: MenuOrder[];
    onDelete(item: MenuOrder): void;
    onQtyChange(ev: any, item: MenuOrder): void;
}


const OrderTable = (props: TableProps) => {
    const placeholderStyle = {
        margin: '0 auto',
        padding: '16px',
    };

    const deleteItem = (item: MenuOrder) => {
        props.onDelete(item);
    };

    const handleQuantityChange = (ev: any, item: MenuOrder) => {
        props.onQtyChange(ev, item);
    };

    const renderPrice = (item: MenuOrder) => {
        return item.quantity * item.menu.price;

    };

    const renderGrandTotal = (item: MenuOrder[]) => {
        let result = 0;
        item.forEach((item) => {
            result = result + item.quantity * item.menu.price
        });
        return result;
    };

    const renderMenuOrderItems = (items: MenuOrder[]) => {
        return items.map((item, k) => {
            return (
                <TableRow key={k}>
                    <TableCell style={{ width: 60 }}>
                        <IconButton
                            aria-label='Delete'
                            style={{ width: 50 }}
                            onClick={() => deleteItem(item)}
                        >
                            <Delete fontSize='inherit' />
                        </IconButton>
                    </TableCell>
                    <TableCell>{item.menu.name}</TableCell>
                    <TableCell>
                        <TextField
                            type='number'
                            InputProps={{ inputProps: { min: 1, max: 10 } }}
                            style={{ width: 75 }}
                            value={item.quantity}
                            onChange={(ev) => handleQuantityChange(ev, item)}
                        />
                    </TableCell>
                    <TableCell>{renderCurrency(item.menu.price)}</TableCell>
                    <TableCell>{renderCurrency(renderPrice(item))}</TableCell>
                </TableRow>
            )
        })

    }

    const renderTableBody = (items: MenuOrder[]) => {
        if (items.length < 1 || items === undefined) {
            return (
                <TableRow style={placeholderStyle}>
                    <TableCell colSpan={6}>
                        <Typography align='center' variant='body2' color='textSecondary'>
                            <i>Please add a Material</i>
                        </Typography>
                    </TableCell>
                </TableRow>
            );
        } else {
            return renderMenuOrderItems(items);
        }
    };

    return (
        <TableContainer component={Paper}>
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <b>Action</b>
                        </TableCell>
                        <TableCell>
                            <b>Menu Name</b>
                        </TableCell>
                        <TableCell>
                            <b>Quantity</b>
                        </TableCell>
                        <TableCell>
                            <b>Menu Price</b>
                        </TableCell>
                        <TableCell>
                            <b>Total</b>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {renderTableBody(props.menuOrder)}
                    <TableRow>
                        <TableCell colSpan={4}>
                        <Typography align='right' variant='body2'>
                            <b>Grand Total</b>
                        </Typography>
                        </TableCell>
                        <TableCell>
                        <b>{renderCurrency(renderGrandTotal(props.menuOrder))}</b>
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}
export default OrderTable;