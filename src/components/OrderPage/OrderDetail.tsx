import React from 'react';
import { MenuOrder } from '../../model/MenuOrder';
import { Order } from '../../model/Order';
import {
    Modal, Card, Fade, CardContent, CardActions, Button, Typography,
    Table, TableCell, TableHead, TableBody, TableFooter, TablePagination, TableRow,
    Divider, TableContainer
} from '@material-ui/core';

import { renderCurrency, renderTime, renderDiscount } from '../../utils/RenderUtils';

interface ModalProps {
    open: boolean;
    order: Order;
    onClose(): void;
}

const OrderDetail = (props: ModalProps) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const modalStyle: React.CSSProperties = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100vh',
    };
    const cardStyle: React.CSSProperties = {
        width: '70%',
        outline: 0,
    };
    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number
    ) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const renderTableItem = (items: MenuOrder[]) => {
        if (items.length > 0) {
            const result = items.map((item, k) => {
                return (
                    <TableRow key={k}>
                        <TableCell>{item.menu.name}</TableCell>
                        <TableCell>{item.quantity}</TableCell>
                        <TableCell>{renderCurrency(item.menu.price)}</TableCell>
                        <TableCell>{renderCurrency(item.quantity * item.menu.price)}</TableCell>
                    </TableRow>
                );
            });
            return result;
        } else {
            return null;
        }
    };
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
        <Fade in={props.open}>
            <Card style={cardStyle}>
                <CardContent>
                    <Typography component='h2' variant='h5'>
                        Order Detail
                    </Typography>
                    <Typography variant='body2'>{`ID: ${props.order.id}`}</Typography>
                    <Typography variant='body2'>{`Time: ${renderTime(
                    props.order.date
                    )}`}</Typography>
                    <Divider style={{ margin: '8px 0px' }} />
                    <Typography component='h2' variant='h6'>
                        Menu Order
                    </Typography>
                    <TableContainer>
                        <Table size='small'>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Quantity</TableCell>
                                    <TableCell>Unit Price</TableCell>
                                    <TableCell>Total</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {renderTableItem(props.order.menuOrder)}
                                {props.order.status === "Done" ? (
                                    <TableRow>
                                    <TableCell colSpan={3}>
                                        <Typography align='right' variant='body2'>
                                            <b>Total:  </b>
                                        </Typography>
                                    </TableCell>
                                    <TableCell>
                                        <b>{renderCurrency(props.order.total)}</b>
                                    </TableCell>
                                </TableRow>
                                ):null}
                                {props.order.status === "Done" ? (
                                    <TableRow>
                                    <TableCell colSpan={3}>
                                        <Typography align='right' variant='body2'>
                                            <b>Tax 15%:  </b>
                                        </Typography>
                                    </TableCell>
                                    <TableCell>
                                        <b>{renderCurrency(props.order.total*0.15)}</b>
                                    </TableCell>
                                </TableRow>
                                ):null}
                                {props.order.status === "Done" ? (
                                    <TableRow>
                                    <TableCell colSpan={3}>
                                        <Typography align='right' variant='body2'>
                                            <b>Discount:  </b>
                                        </Typography>
                                    </TableCell>
                                    <TableCell>
                                        <b>{renderDiscount(props.order.promo.discount)}</b>
                                    </TableCell>
                                </TableRow>
                                ):null}
                                {props.order.status === "Done" ?
                                    (
                                        <TableRow>
                                             <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Grand Total:  </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.grandTotal)}</b>
                                            </TableCell>
                                        </TableRow>
                                        
                                    ):(
                                        <TableRow>
                                            <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Total:  </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.total)}</b>
                                            </TableCell>
                                        </TableRow>
                                )}
                                {props.order.status !== "Done" ? (
                                    <TableRow>
                                            <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Tax 15% :  </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.total*0.15)}</b>
                                            </TableCell>
                                    </TableRow>
                                ):null}
                                 {props.order.status !== "Done" ? (
                                    <TableRow>
                                            <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Grand Total:  </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.total+props.order.total*0.15)}</b>
                                            </TableCell>
                                    </TableRow>
                                ):null}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TableCell colSpan={4}>
                                    <TablePagination
                                        rowsPerPageOptions={[5, 10, 25]}
                                        component='div'
                                        count={props.order.menuOrder.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                    />
                                    </TableCell>
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </TableContainer>
                </CardContent>
                <CardActions>
                    <Button variant='text' onClick={props.onClose} style={{ marginLeft: 'auto' }}>
                    <b>Close</b>
                    </Button>
                </CardActions>
            </Card>
        </Fade>

    </Modal>
    )
}

export default OrderDetail;