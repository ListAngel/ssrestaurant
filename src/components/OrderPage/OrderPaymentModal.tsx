import React from 'react';
import {
    Modal,
    Fade,
    Card,
    CardContent,
    Typography,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Grid,
    Tooltip,
    CardActions,
    Button,
    TextField,
} from '@material-ui/core';
import { Check } from '@material-ui/icons';
import { Order } from '../../model/Order';
import {Promo} from '../../model/Promo';
import { MenuOrder } from '../../model/MenuOrder';
import {orderPayment} from '../../controller/OrderController';
import { renderCurrency, renderDiscount } from '../../utils/RenderUtils';
import { checkPromo } from '../../controller/PromoController';
import { Theme, makeStyles } from "@material-ui/core/styles";
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';

const useStyles = makeStyles((theme: Theme) => ({
    btnPromo: {
        marginTop: theme.spacing(2),
        marginLeft: theme.spacing(2),
    }
}));
interface ModalProps {
    open: boolean;
    onClose(): void;
    order: Order;
}

const OrderPaymentModal = (props: ModalProps) => {
    const defaultPromo: Promo = {
        id: "",
        codePromo: "",
        promoTitle: "",
        startDate: 0,
        expiredDate: 0,
        discount: 0,
    }
    const [promoCode, setPromoCode] = React.useState('');
    const [promo, setPromo] = React.useState<Promo>(defaultPromo);
    const [discount, setDiscount] = React.useState(0);
    const [promoHelperText, setPromoHelperText] = React.useState('');
    const [promoError, setPromoError] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const classes = useStyles();

    const modalStyle: React.CSSProperties = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100vh',
    };

    const cardStyle: React.CSSProperties = {
        width: '50%',
        outline: 0,
    };
    
    const handleCheckPromo = async () => {
        const result = await checkPromo(promoCode);
        if (result === 0) {
            setPromoHelperText('Promo not found!');
            setPromoError(true);
        } else {
            setPromo(result);
            setDiscount(result.discount);
            setPromoHelperText('Promo applied!');
            setPromoError(false);
        }
    }
    const renderTableItem = (items: MenuOrder[]) => {
        if (items.length > 0) {
            const result = items.map((item, k) => {
                return (
                    <TableRow key={k}>
                        <TableCell>{item.menu.name}</TableCell>
                        <TableCell>{item.quantity}</TableCell>
                        <TableCell>{renderCurrency(item.menu.price)}</TableCell>
                        <TableCell>{renderCurrency(item.quantity * item.menu.price)}</TableCell>
                    </TableRow>
                );
            });
            return result;
        } else {
            return null;
        }
    };
    
    const handleClose = () => {
        props.onClose();
        setPromoCode('');
        setPromoError(false);
        setPromoHelperText('');
        setDiscount(0);
    };

    const handlePayment = async () => {
        setLoading(true);
        const checker = await orderPayment(props.order, props.order.total +
                                                            props.order.total * 0.15 -
                                                            (props.order.total + props.order.total * 0.15) 
                                                            * discount, promo);
       if(checker === true){
            setLoading(false);
            window.location.reload();
        }else {
            setLoading(false);
            window.alert(`Can't Payment`);
        }

    }
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <Fade in={props.open}>
                <Card style={cardStyle}>
                    <CardContent>
                        <Grid
                            container
                            justify='flex-start'
                            direction='column'
                            spacing={5}
                            style={{ padding: 16 }}
                        >
                            <Typography variant='h5'>Make Payment</Typography>
                            <Typography variant='body2'>
                                Order ID: {props.order.id}
                            </Typography>
                            <Typography variant='body2'>
                                PIC Name: {props.order.pic}
                            </Typography>
                            <Typography variant='body2'>
                                Table Number: {props.order.table.tableNumber}
                            </Typography>
                            <TableContainer>
                                <Table size='small'>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Name</TableCell>
                                            <TableCell>Quantity</TableCell>
                                            <TableCell>Unit Price</TableCell>
                                            <TableCell>Total</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {renderTableItem(props.order.menuOrder)}
                                        <TableRow>
                                            <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Total: </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.total)}</b>
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell colSpan={3}>
                                                <Typography align='right' variant='body2'>
                                                    <b>Tax (10%): </b>
                                                </Typography>
                                            </TableCell>
                                            <TableCell>
                                                <b>{renderCurrency(props.order.total * 0.1)}</b>
                                            </TableCell>
                                        </TableRow>
                                        {discount > 0 ?
                                            (
                                                <TableRow>
                                                    <TableCell colSpan={3}>
                                                        <Typography align='right' variant='body2'>
                                                            <b>Discount:  </b>
                                                        </Typography>
                                                    </TableCell>
                                                    <TableCell>
                                                        <b>{renderDiscount(discount)}</b>
                                                    </TableCell>
                                                </TableRow>
                                            )
                                            : null}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <Grid container justify='space-between' alignItems='center' direction='row'>
                                <Grid item>
                                    <TextField
                                        label='Promo Code'
                                        type='text'
                                        style={{ width: 150 }}
                                        value={promoCode}
                                        onChange={(ev) => {
                                            setPromoCode(ev.target.value);
                                        }}
                                        helperText={promoHelperText}
                                        error={promoError}
                                    />
                                    <Tooltip title='CheckPromo' arrow>
                                        <Button
                                            className={classes.btnPromo}
                                            variant="contained"
                                            color="primary"
                                            startIcon={<Check />}
                                            onClick={handleCheckPromo}
                                        >
                                            Check Promo
                                </Button>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                            <Grid item>
                                <Typography
                                    variant='body2'
                                    align='right'
                                    style={{ fontSize: '1.1rem' }}
                                >
                                    <b>
                                        Grand Total:{' '}
                                        {renderCurrency(
                                            props.order.total +
                                            props.order.total * 0.15 -
                                            (props.order.total + props.order.total * 0.15) * discount
                                        )}
                                    </b>
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions style={{ padding: 16 }}>
                        <Button variant='text' onClick={handleClose} style={{ marginLeft: 'auto' }}>
                            <b>Close</b>
                        </Button>
                        <Button
                            variant='contained'
                            disabled={promoError}
                            color='primary'
                            onClick={() => handlePayment()}
                        >
                            <b>Submit</b>
                        </Button>
                    </CardActions>
                    <FullScreenSpinner open={loading} />
                </Card>
            </Fade>
        </Modal>
    )
}

export default OrderPaymentModal;