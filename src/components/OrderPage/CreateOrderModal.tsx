import React from 'react';
import {
    Modal, Fade, CardContent, Button, Typography,
    Table, TableCell, TableHead, TableBody, TableRow,
    Divider, TableContainer, Container, TextField, Select, MenuItem
} from '@material-ui/core';
import Add from '@material-ui/icons/Add';
import { MenuOrder } from '../../model/MenuOrder';
import { Order } from '../../model/Order';
import { Tables } from '../../model/Tables';
import { Promo } from '../../model/Promo';
import { Reservation } from '../../model/Reservation';
import { renderCurrency, renderTime } from '../../utils/RenderUtils';
import { createOrder, createOrderReservation } from '../../controller/OrderController';
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import { useHistory } from 'react-router-dom';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: theme.spacing(3),
            backgroundColor: '#063852',
            color: '#90AFC5'
        },
        formStyle: {
            paddingTop: theme.spacing(2)
        },
        buttonStyle: {
            marginTop: theme.spacing(20),
            alignSelf: 'center',
        },
        select: {
            width: '100%',
            marginBottom: theme.spacing(2),

        }

    })
);

interface ModalProps {
    open: boolean;
    menuOrder: MenuOrder[];
    listTable: Tables[];
    orderReservation?: Reservation;
    onClose(): void;
}

const CreateOrderModal = (props: ModalProps) => {
    const defaultTable: Tables = {
        id: 'null',
        tableNumber: "",
        type: "",
        capacity: 0,
        status: "",
    }
    const defaultPromo: Promo = {
        id: "",
        codePromo: "",
        promoTitle: "",
        startDate: 0,
        expiredDate: 0,
        discount: 0,
    }
    const [picName, setPicName] = React.useState("");
    const [tableId, setTableId] = React.useState("");
    const [tableSelected, setTableSelected] = React.useState<Tables>(defaultTable);
    const [loading, setLoading] = React.useState(false);
    const classes = useStyles();
    const history = useHistory();
    const modalStyle: React.CSSProperties = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100vh',
    };

    const renderTableItem = (items: MenuOrder[]) => {
        console.log("aaaas", props.orderReservation)
        if (items.length > 0) {
            const result = items.map((item, k) => {
                return (
                    <TableRow key={k}>
                        <TableCell>{item.menu.name}</TableCell>
                        <TableCell>{renderCurrency(item.menu.price)}</TableCell>
                        <TableCell>{item.quantity}</TableCell>
                        <TableCell>{renderCurrency(item.quantity * item.menu.price)}</TableCell>
                    </TableRow>
                );
            });
            return result;
        } else {
            return null;
        }
    }
    const renderTable = () => {
        return (
            <TableContainer>
                <Table size='small'>
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Price</TableCell>
                            <TableCell>Qauntity</TableCell>
                            <TableCell>Total</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {renderTableItem(props.menuOrder)}
                    </TableBody>
                </Table>
            </TableContainer>
        )
    }
    const handleTableSelected = (ev: any) => {
        setTableId(ev.target.value);
        props.listTable.forEach((doc) => {
            if (doc.id === ev.target.value) {
                setTableSelected(doc);
            }
        })
    }
    const renderListTable = (listTable: Tables[]) => {
        return (
            <Select
                className={classes.select}
                style={{ marginTop: 20 }}
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={tableId}
                onChange={handleTableSelected}
            >
                {listTable.map((item) => (
                    <MenuItem key={item.tableNumber} value={item.id}>
                        {item.tableNumber}
                    </MenuItem>
                ))}

            </Select>
        )
    }

    const handleGrandTotal = (items: MenuOrder[]) => {
        let result = 0;
        if (items.length > 0) {
            items.forEach((doc) => {
                result = result + (doc.quantity * doc.menu.price);
            })
        }
        return result;
    }

    const handleOrder = async () => {
        setLoading(true);
        const today = new Date();

        const newOrder: Order = {
            pic: picName,
            menuOrder: props.menuOrder,
            table: tableSelected,
            status: "On Process",
            date: today.getTime(),
            promo: defaultPromo,
            total: handleGrandTotal(props.menuOrder),
            grandTotal: 0,
        }
        const checker = await createOrder(newOrder);
        if (checker === true) {
            setLoading(false);
            history.push('/order');
        } else {
            setLoading(false);
            window.alert(`Can't Create Purchasing`);
        }
    }

    const hanldeOrderReservation = async () => {
        setLoading(true);
        const today = new Date();
        if(props.orderReservation !== undefined){
            const newOrder: Order = {
                pic: props.orderReservation.pic,
                menuOrder: props.menuOrder,
                table: props.orderReservation.tables,
                status: "Process Reserved",
                date: props.orderReservation.reservationDate,
                promo: defaultPromo,
                total: handleGrandTotal(props.menuOrder),
                grandTotal: 0,
                reservation: props.orderReservation
            }
            const checker = await createOrderReservation(newOrder);
            if (checker === true) {
                setLoading(false);
                history.push('/order');
            } else {
                setLoading(false);
                window.alert(`Can't Create Purchasing`);
            }
        }
       
    }
    return (
        <Modal open={props.open} onClose={props.onClose} style={modalStyle}>
            <Fade in={props.open}>
                <Container maxWidth="sm" className={classes.root}>
                    <CardContent>
                        <Typography component='h1' variant='h4'>
                            Create Order
                        </Typography>
                        <Divider style={{ margin: '8px 0px' }} />
                        {console.log("ORder", props.orderReservation?.pic)}
                        {props.orderReservation?.pic !== undefined ? (
                            <div>
                                <Typography component='h6' variant='h6'>
                                    Reservation Date: {renderTime(props.orderReservation?.reservationDate)}
                                </Typography>
                                <Typography component='h6' variant='h6'>
                                    Name: {props.orderReservation?.pic}
                                </Typography>
                                <Typography component='h6' variant='h6'>
                                    Table Number: {props.orderReservation?.tables.tableNumber}
                                </Typography>
                                <Typography component='h6' variant='h6'>
                                    Capacity : {props.orderReservation?.tables.capacity}
                                </Typography>
                            </div>

                        ) : (
                                <TextField
                                    fullWidth
                                    label='Customer Name'
                                    variant='outlined'
                                    value={picName}
                                    onChange={(ev) => setPicName(ev.target.value)}
                                />
                            )}
                        {props.orderReservation?.pic === undefined ? (
                            renderListTable(props.listTable)
                        ) : null}

                        {props.orderReservation?.pic === undefined && tableId !== "" ? (
                            <Typography>
                                Capacity: {tableSelected?.capacity}
                            </Typography>
                        ) : null}

                    </CardContent>

                    <CardContent>
                        <Typography component='h2' variant='h5'>
                            List Menu Order
                        </Typography>
                        {renderTable()}
                    </CardContent>
                    <CardContent>
                        <Typography
                            variant='body2'
                            align='right'
                            style={{ fontSize: '1.1rem' }}
                        >
                            <b>
                                Grand Total:{' '}
                                {renderCurrency(handleGrandTotal(props.menuOrder))}
                            </b>
                        </Typography>
                    </CardContent>
                    {props.menuOrder.length !== 0 && tableId !== "" && picName !== "" ? (
                        <Button
                            variant='text'
                            color='primary'
                            startIcon={<Add />}
                            onClick={handleOrder}
                        >
                            <b>Order</b>
                        </Button>
                    ) : null}
                    {props.menuOrder.length !== 0 && props.orderReservation?.pic !== undefined ? (
                        <Button
                            variant='text'
                            color='primary'
                            startIcon={<Add />}
                            onClick={hanldeOrderReservation}
                        >
                            <b>Order</b>
                        </Button>
                    ) : null}
                    <FullScreenSpinner open={loading} />
                </Container>
            </Fade>
        </Modal>
    )
}


export default CreateOrderModal;