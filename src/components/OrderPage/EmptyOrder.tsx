import React from 'react';
import { Card, CardContent, Typography, CardActions, Button, Container } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

const EmptyOrder = () => {
    const history = useHistory();

    const containerStyle = {
        display: 'flex',
        justifyContent: 'center',
    };

    const createOrder = () => {
        history.push('/');
    }

    return (
        <Container style={containerStyle}>
            <Card style={{ maxWidth: 300 }}>
                <CardContent>
                    <Typography gutterBottom variant='h5' component='h2'>
                        Restaurant Order
                    </Typography>
                    <Typography variant='body2' color='textSecondary' component='p'>
                        Create Your Order for Your Favorite Menus
                    </Typography>
                </CardContent>
                
                <CardActions>
                    <Button
                        size='small'
                        color='primary'
                        style={{ fontWeight: 'bold', marginLeft: 'auto' }}
                        onClick={createOrder}
                    >
                        Create
                    </Button>
                </CardActions>

            </Card>
        </Container>

    )
}

export default EmptyOrder;