import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import {
    TableContainer,
    Paper,
    IconButton,
    Table,
    TableBody,
    TableHead,
    Tooltip,
    TableRow,
    TableCell,
    Button,
    Container,
    Typography,
    TextField,
    Select,
    MenuItem
} from '@material-ui/core';
import { Order } from '../../model/Order';
import { Tables } from '../../model/Tables';
import { Promo } from '../../model/Promo';
import { useHistory } from 'react-router-dom';
import ViewListIcon from '@material-ui/icons/ViewList';
import UpdateIcon from '@material-ui/icons/Update';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import EmptyOrder from './EmptyOrder';
import { getAllOrder } from '../../controller/OrderController';
import { Add } from '@material-ui/icons';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import { renderTime, renderCurrency } from '../../utils/RenderUtils';
import DeleteOrderModal from '../../components/OrderPage/DeleteOrderModal';
import OrderDetail from '../../components/OrderPage/OrderDetail';
import OrderPaymentModal from './OrderPaymentModal'
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import {Reservation} from '../../model/Reservation';

const StyledTableCell = withStyles(() => ({
    head: {
        backgroundColor: 'gray',
        color: 'white',
        fontSize: 20,
    },
}))(TableCell);

const TableHeader = () => {
    return (
        <TableHead>
            <TableRow>
                <StyledTableCell style={{ width: 200, textAlign: 'center' }}>
                    Actions
                </StyledTableCell>
                <StyledTableCell>Date </StyledTableCell>
                <StyledTableCell>PIC Name</StyledTableCell>
                <StyledTableCell>Table</StyledTableCell>
                <StyledTableCell>Total</StyledTableCell>
                <StyledTableCell>Status</StyledTableCell>
            </TableRow>
        </TableHead>
    );
};

const useStyles = makeStyles(theme => ({
    root: {
        paddingBottom: theme.spacing(8),
    },
    paper: {
        marginTop: theme.spacing(5),
    },
    done: {
        color: '#64dd17',
        marginLeft: theme.spacing(2),

    },
    buttonIcon: {
        marginLeft: theme.spacing(7),
    },
    search: {
        marginTop: theme.spacing(3),
        height: 55,
    },
    select: {
        height: 55,
        marginLeft: theme.spacing(3),

    }
}))

const ShowOrderPage = () => {
    const defaultTable: Tables = {
        id: 'null',
        tableNumber: "",
        type: "",
        capacity: 0,
        status: "",
    }
    const defaultPromo: Promo = {
        id: "",
        codePromo: "",
        promoTitle: "",
        startDate: 0,
        expiredDate: 0,
        discount: 0,
    }
    const defaultReservation: Reservation = {
        id: "",
        pic: "",
        tables: defaultTable,
        reservationCreated: 0,
        reservationDate: 0,
        status: "",
    }
    const defaultOrder: Order = {
        id: "",
        pic: "",
        menuOrder: [],
        table: defaultTable,
        status: "",
        date: 0,
        promo: defaultPromo,
        total: 0,
        grandTotal: 0,
        reservation: defaultReservation,
    }
    const [orderSelected, setOrderSelected] = React.useState<Order>(defaultOrder);
    const history = useHistory();
    const classes = useStyles();
    const [order, setOrder] = React.useState([] as any[]);
    const [filterOrder, setFilterOrder] = React.useState([] as any[]);
    const [tempFilterOrder, setTempFilterOrder] = React.useState([] as any[]);
    const [loading, setLoading] = React.useState(true);
    const [txtSearch, setTxtSearch] = React.useState('');
    const [status, setStatus] = React.useState('All');

    const [modalDelete, setModalDelete] = React.useState(false);
    const [modalDetailOrder, setModalDetailOrder] = React.useState(false);
    const [modalPayment, setModalPayment] = React.useState(false);

    const createOrder = () => {
        history.push('/');
    }
    const showOrderModal = (item: Order) => {
        setOrderSelected(item);
        setModalDetailOrder(true);
    }
    const updateOrderModal = (item: Order) => {
        history.push(`/order/edit/${item.id}`);
    }
    const deleteOrderModal = (item: Order) => {
        setOrderSelected(item);
        setModalDelete(true);
    }
    const showPaymentModal = (item: Order) => {
        console.log(item.reservation);
        setOrderSelected(item);
        setModalPayment(true);
    }
    const renderTableBody = (items: Order[]) => {
        const tableBody = items.map((item: Order, k) => {
            return (
                <TableRow hover tabIndex={-1} key={item.id}>
                    <TableCell>
                        {item.status === 'On Process' || item.status === "Process Reserved" ? (
                            <div>
                                <Tooltip title='View Order' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            showOrderModal(item);
                                        }}
                                    >
                                        <ViewListIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title='Update Order' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            updateOrderModal(item);
                                        }}
                                    >
                                        <UpdateIcon />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title='Delete Order' arrow>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            deleteOrderModal(item);
                                        }}
                                    >
                                        <DeleteIcon />
                                    </IconButton>
                                </Tooltip>
                            </div>

                        ) : (
                                <Tooltip title='View Purchasing' arrow className={classes.buttonIcon}>
                                    <IconButton
                                        aria-label='List'
                                        style={{ width: 50 }}
                                        onClick={() => {
                                            showOrderModal(item);
                                        }}
                                    >
                                        <ViewListIcon />
                                    </IconButton>
                                </Tooltip>
                            )}

                    </TableCell>
                    <TableCell>{renderTime(item.date)}</TableCell>
                    <TableCell>{item.pic}</TableCell>
                    <TableCell>{item.table.tableNumber}</TableCell>
                    {item.status === 'Done' ? (
                        <TableCell>{renderCurrency(Number(item.grandTotal))}</TableCell>
                    ) : (
                            <TableCell>{renderCurrency(Number(item.total + item.total * 0.15))}</TableCell>
                        )}

                    <TableCell>
                        {item.status === 'On Process' ? (
                            <Tooltip title='Payment' arrow>
                                <Button
                                    variant="contained"
                                    color="secondary"
                                    startIcon={<AccountBalanceWalletIcon />}
                                    onClick={() => {
                                        showPaymentModal(item);
                                    }}
                                >
                                    Payment
                                </Button>
                            </Tooltip>
                        ) : null}
                        {item.status === "Process Reserved" ? (
                            <Tooltip title='Payment Order Reservation' arrow>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    startIcon={<AccountBalanceWalletIcon />}
                                    onClick={() => {
                                        showPaymentModal(item);
                                    }}
                                >
                                    Reservation
                                </Button>
                            </Tooltip>
                        ) : null}
                        {item.status === "Done" ? (
                            <div className={classes.done}>
                                <CheckCircleOutlineIcon />
                                <Typography>
                                    Done
                             </Typography>
                            </div>
                        ) : null}
                         {item.status === "Reserved" ? (
                            <div className={classes.done}>
                                <CheckCircleOutlineIcon />
                                <Typography>
                                    Reserved
                             </Typography>
                            </div>
                        ) : null}
                    </TableCell>

                </TableRow>
            )
        })
        return tableBody;
    }

    // Get All Promo With Contoller Promo
    React.useEffect(() => {
        getAllOrder().then(result => {
            setOrder(result);
            setTempFilterOrder(result);
            setFilterOrder(result);
            setLoading(false);
        })
    }, []);

    if (order.length < 1 && loading === false) {
        return (
            <div>
                <EmptyOrder />
            </div>
        );
    }
    const handleSearchPurchasing = (ev: any) => {
        setTxtSearch(ev.target.value);
        const temp = tempFilterOrder.filter((item: any) =>
            item.pic.toLowerCase().includes(ev.target.value.toLowerCase())
        );
        setFilterOrder(temp);
    };

    const handleStatusActive = (ev: any) => {
        setStatus(ev.target.value);

        if (ev.target.value === "All") {
            setFilterOrder(order);
            setTempFilterOrder(order);
        } else if (ev.target.value === "On Process") {
            const temp = order.filter((item: any) => {
                return (item.status === 'On Process')
            });

            setFilterOrder(temp);
            setTempFilterOrder(temp);
        } else if ((ev.target.value === "Done")){
            const temp = order.filter((item: any) => {
                return item.status === 'Done'
            });
            setFilterOrder(temp);
            setTempFilterOrder(temp);
        }else {
            const temp = order.filter((item: any) => {
                return item.status === 'Reserved'
            });
            setFilterOrder(temp);
            setTempFilterOrder(temp);
        }
    }

    const handleCloseModal = () => {
        setModalDetailOrder(false);
    };

    return (
        <Container maxWidth="md" className={classes.root} >
            <Typography variant='h4' component='h4'>
                Order Management
            </Typography>
            <form>
                <TextField
                    className={classes.search}
                    label='Search Purchasing'
                    variant='outlined'
                    value={txtSearch}
                    onChange={(ev) => handleSearchPurchasing(ev)}
                />
                <Select
                    className={classes.select}
                    style={{ marginTop: 20 }}
                    labelId="demo-simple-select-helper-label"
                    id="demo-simple-select-helper"
                    value={status}
                    onChange={handleStatusActive}
                >
                    <MenuItem value={"All"}>All</MenuItem>
                    <MenuItem value={"Done"}>Done</MenuItem>
                    <MenuItem value={"On Process"}>On Process</MenuItem>
                    <MenuItem value={"Reserved"}>Reserved</MenuItem>
                </Select>
                <Button
                    variant='contained'
                    startIcon={<Add />}
                    color='primary'
                    style={{ height: 55, marginLeft: 20 }}
                    onClick={createOrder}
                >
                    Add New ORder
                    </Button>
            </form>
            <Paper className={classes.paper}>
                <TableContainer>
                    <Table>
                        <TableHeader />
                        <TableBody>{renderTableBody(filterOrder)}</TableBody>
                    </Table>
                </TableContainer>
            </Paper>
            <DeleteOrderModal
                open={modalDelete}
                orderDeleted={orderSelected}
                onClose={(open) => {
                    setModalDelete(open)
                }}
            />
            <OrderDetail
                open={modalDetailOrder}
                order={orderSelected}
                onClose={() => {
                    handleCloseModal();
                }}
            />
            <OrderPaymentModal
                open={modalPayment}
                order={orderSelected}
                onClose={() => setModalPayment(false)}
            />
            <FullScreenSpinner open={loading} />
        </Container>
    )
}


export default ShowOrderPage;