import React from 'react';
import {
    Container,
    Typography,
    TextField,
    Button,
} from "@material-ui/core";
import { Add } from '@material-ui/icons';
import CancelIcon from '@material-ui/icons/Cancel';
import { Theme, makeStyles } from "@material-ui/core/styles";
import { updateOrder, getOrderWithId } from '../../controller/OrderController';
import { Tables } from '../../model/Tables';
import OrderTable from './OrderTable';
import AddMenuModal from '../../components/MenuPage/AddMenuModal';
import { MenuOrder } from '../../model/MenuOrder';
import { Order } from '../../model/Order';
import { Menu } from '../../model/Menu';
import {Promo} from '../../model/Promo';
import { getAllMenu } from '../../controller/MenuController';
import FullScreenSpinner from '../../components/Loading/FullScreenSpinner';
import { useHistory, useParams } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: '100%',
        width: '75%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'start',
        alignItems: 'stretch',
        marginTop: theme.spacing(5),
    },
    form: {
        width: '80%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        // flexWrap: 'wrap',
        padding: '16px 0',
    },
    buttonStyle: {
        width: '30%',
        backgroundColor: '#0063B2FF',
        color: '#9CC3D5FF',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
    },
    buttonEnd: {
        alignSelf: 'center',
        marginTop: theme.spacing(3),
    }
}));

const EditOrder = () => {
    const defaultTable: Tables = {
        id: 'null',
        tableNumber: "",
        type: "",
        capacity: 0,
        status: "",
    }

    const defaultPromo: Promo = {
        id: "",
        codePromo: "",
        promoTitle: "",
        startDate: 0,
        expiredDate: 0,
        discount: 0,
    }
    const defaultOrder: Order = {
        id: "",
        pic: "",
        menuOrder: [],
        table: defaultTable,
        status: "",
        date: 0,
        promo: defaultPromo,
        total: 0,
        grandTotal: 0,
    }
    const classes = useStyles();
    const [menuList, setMenuList] = React.useState<Menu[]>([]);
    const [idOrder, setIdOrder] = React.useState('');
    const [picName, setPicName] = React.useState("");
    const [menuOrder, setMenuOrder] = React.useState<MenuOrder[]>([]);
    const [tableSelected, setTableSelected] = React.useState<Tables>(defaultTable);
    const [oldOrder, setOldOrder] = React.useState<Order>(defaultOrder);
    const [loading, setLoading] = React.useState(false);
    const [modalOpen, setModalOpen] = React.useState(false);
    const history = useHistory();
    const { id } = useParams();

    async function fetchMenu() {
        const result = await getAllMenu();
        setMenuList(result);
    }
    const fetchOrder = async (orderId: string) => {
        await getOrderWithId(orderId).then((doc) => {
            setTableSelected(doc.table);
            setIdOrder(orderId);
            setPicName(doc.pic);
            setMenuOrder(doc.menuOrder);
            setOldOrder(doc);
        })
    }
    React.useEffect(() => {
        fetchMenu();
        fetchOrder(id || 'null');
        // eslint-disable-next-line
    }, []);

    if (menuList.length < 1) {
        return <FullScreenSpinner open={true} />;
    }

    const removeMenu = (item: MenuOrder) => {
        const temp = [...menuOrder];
        const index = temp.indexOf(item);
        if (index !== -1) {
            temp.splice(index, 1);
            setMenuOrder(temp);
        }
    };

    const handleQtyChange = (ev: any, item: MenuOrder) => {
        const temp = [...menuOrder];
        const index = temp.indexOf(item);
        temp[index].quantity = ev.target.value as number;

        setMenuOrder(temp);
    };
    //Add selected menu from modal into table (state items)
    const addSelectedMenu = (item: Menu) => {
        // const temp = [...items, item];
        // setItems(temp);
        const newMenuOrder: MenuOrder = {
            menu: item,
            quantity: 1,

        };
        const temp = [...menuOrder, newMenuOrder];
        setMenuOrder(temp);
    };
    const addMenuOrder = () => {
        setModalOpen(true);
        const mOrders: { menu: Menu; quantity: number }[] = [];
        menuOrder.forEach((item) => {
            mOrders.push({
                menu: item.menu,
                quantity: item.quantity,
            })
        });

    }

    const btnGroupStyle: React.CSSProperties = {
        width: '100%',
        display: 'flex',
        justifyContent: 'flex-end',
    };

    const editOrder = async () => {
        setLoading(true);

        const mOrders: { menu: Menu; quantity: number }[] = [];
        const today = new Date();
        let tempTotal = 0;

        menuOrder.forEach((item) => {
            tempTotal = tempTotal + (item.menu.price * item.quantity);
            mOrders.push({
                menu: item.menu,
                quantity: Number(item.quantity),
            })
        })
        const defaultPromo: Promo = {
            id: "",
            codePromo: "",
            promoTitle: "",
            startDate: 0,
            expiredDate: 0,
            discount: 0,
        }
        const newOrder: Order = {
            pic: picName,
            menuOrder: mOrders,
            table: tableSelected,
            status: "On Process",
            date: today.getTime(),
            total: tempTotal,
            grandTotal: 0,
            promo: defaultPromo,
        }

        const checker = await updateOrder(idOrder, newOrder, oldOrder);
        if (checker === true) {
            setLoading(false);
            history.push('/order');
        } else {
            setLoading(false);
            window.alert(`Can't Create Purchasing`);
        }
    }

    return (
        <Container className={classes.root}>
            <div style={{ flexGrow: 1 }}>
                <Typography variant='h4'>
                    Edit Order
                </Typography>
            </div>
            <form className={classes.form}>
                <TextField
                    fullWidth
                    label='PIC Name'
                    variant='outlined'
                    value={picName}
                    onChange={(ev) => setPicName(ev.target.value)}
                />
                <Button
                    variant="text"
                    startIcon={<Add />}
                    disableRipple
                    className={classes.buttonStyle}
                    onClick={addMenuOrder}
                >
                    <b>Add Menu</b>
                </Button>
                <OrderTable
                    menuOrder={menuOrder}
                    onDelete={(item) => removeMenu(item)}
                    onQtyChange={(ev, item) => handleQtyChange(ev, item)}
                />
                <AddMenuModal
                    open={modalOpen}
                    menuList={menuList}
                    onClose={() => {
                        setModalOpen(false);
                    }}
                    onMenuAdd={(item) => {
                        addSelectedMenu(item);
                    }}
                />
                <div style={btnGroupStyle}>
                    <Button
                        variant='text'
                        color='secondary'
                        startIcon={<CancelIcon />}
                        disableRipple
                        className={classes.buttonEnd}
                        onClick={() => {
                            history.push('/order');
                        }}
                    >
                        <b>Cancel</b>
                    </Button>
                    <Button
                        variant='text'
                        color='primary'
                        startIcon={<Add />}
                        disableRipple
                        className={classes.buttonEnd}
                        disabled={
                            menuOrder.length < 1 ||
                            picName.length < 3
                        }
                        onClick={editOrder}
                    >
                        <b>Edit Order</b>
                    </Button>
                </div>
            </form>
            <FullScreenSpinner open={loading} />
        </Container>
    )
}

export default EditOrder;