import React from 'react';
import EditTablesModal from '../../../components/TablePage/EditTablesModal';

const TableEdit = () => { 
    return (
        <EditTablesModal />
    )
}

export default TableEdit;