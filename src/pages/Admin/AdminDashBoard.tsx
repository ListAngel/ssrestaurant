import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { Container,AppBar, Tabs, Tab, Typography, Box } from '@material-ui/core';
import ShowCardMenuList from '../../components/MenuPage/ShowCardMenuList';
import ShowPromo from '../../components/PromoPage/ShowPromo';
import ShowTable from '../../components/TablePage/ShowTable';
import ShowPurchasing from '../../components/PurchasingPage/ShowPurchasing';
interface TabPanelProps {
    children?: React.ReactNode;
    dir?: string;
    index: any;
    value: any;
}


const TabPanel = (props: TabPanelProps) => {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

function a11yProps(index: any) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        marginTop: theme.spacing(5),
    },
    appbar: {
        marginTop: theme.spacing(5),
    },
}));

const AdminDashBoard = () => {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index: number) => {
        setValue(index);
    };
    return (
        <Container maxWidth="md" className={classes.root} >
             <Typography variant='h4' component='h4'>
                    Admin DashBoard
            </Typography>
            <AppBar position="static" color="default" className={classes.appbar}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Menu" {...a11yProps(0)} />
                    <Tab label="Promo" {...a11yProps(1)} />
                    <Tab label="Table" {...a11yProps(2)} />
                    <Tab label="Purchasing" {...a11yProps(3)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <ShowCardMenuList/>
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <ShowPromo/>
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <ShowTable/>
                </TabPanel>
                <TabPanel value={value} index={3} dir={theme.direction}>
                    <ShowPurchasing/>
                </TabPanel>
            
            </SwipeableViews>
        </Container>
    );
}


export default AdminDashBoard;