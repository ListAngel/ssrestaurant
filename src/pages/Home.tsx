import * as React from 'react';
import Header from '../components/Home/Header';
import ListMenu from '../components/Home/ListMenu';


const Home = () => {
  return (
    <div>
      <Header/>
      <ListMenu/>
    </div>
  );
};

export default Home;
