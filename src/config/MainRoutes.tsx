import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import Login from '../pages/Login';
import Home from '../pages/Home';
import MenuEdit from '../pages/Admin/Menu/MenuEdit';
import PromoEdit from '../pages/Admin/Promo/PromoEdit';
import TableEdit from '../pages/Admin/Table/TableEdit';
import CreatePurchasingPage from '../pages/Admin/Purchasing/CreatePurchasingPage';
import EditPurchasingPage from '../pages/Admin/Purchasing/EditPurchasingPage';
import OrderPage from '../pages/Order/OrderPage';
import EditOrderPage from '../pages/Order/EditOrderPage';
import AdminDashBoard from '../pages/Admin/AdminDashBoard';
import ReservationPage from '../pages/Reservation/ReservationPage';
import ReservationCreatePage from '../pages/Reservation/ReservationCreatePage';
import ReservationEditPage from '../pages/Reservation/ReservationEditPage';
export default class MainRoutes extends React.Component {
  render() {
    return (
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/admin/menu/edit/:id' component={MenuEdit}/>
          
          <Route exact path='/admin/promo/edit/:id' component={PromoEdit}/>
          
          <Route exact path='/admin/tables/edit/:id' component={TableEdit}/>
          
          <Route exact path='/admin/purchasing/create' component={CreatePurchasingPage}/>
          <Route exact path='/admin/purchasing/edit/:id' component={EditPurchasingPage}/>

          <Route exact path='/order' component={OrderPage}/>
          <Route exact path='/order/edit/:id' component={EditOrderPage}/>

          <Route exact path='/reservation' component={ReservationPage}/>
          <Route exact path='/reservation/create' component={ReservationCreatePage}/>
          <Route exact path='/reservation/edit/:id' component={ReservationEditPage}/>
          <Route exact path='/admindashboard' component={AdminDashBoard}/>
          <Route exact path='/:idRvn' component={Home} />
          
          
        </Switch>
    );
  }
}



