import { Menu, dbMenu } from '../model/Menu';
import firebase from 'firebase';


export const getAllMenu = async () => {
    const result: Menu[] = [];
    await dbMenu()
        .get()
        .then((querySnapShot) => {
            querySnapShot.forEach((doc) => {
                const data = doc.data();
                const menuItem: Menu = {
                    id: doc.id,
                    name: data.name,
                    price: data.price,
                    type: data.type,
                    imageName: data.imageName,
                    urlImage: data.urlImage,
                    stock: data.stock,
                }
                result.push(menuItem);
            })
        })
    return result;
}
export const getMenuById = async (menuId: string) => {
    let result: Menu = {} as Menu;
    await dbMenu()
        .doc(`${menuId}`)
        .get()
        .then((doc) => {
            const newMenu: Menu = {
                id: menuId,
                name: doc.data()?.name,
                type: doc.data()?.type,
                price: doc.data()?.price,
                imageName: doc.data()?.imageName,
                urlImage: doc.data()?.urlImage,
                stock: doc.data()?.stock,
            }
            result = newMenu;
        })
    return result;
}

export const updateMenu = async (idMenu: string, image: any, menu: Menu) => {
    let check = true;
    let selfMenuName = "";
    await dbMenu().doc(`${idMenu}`).get().then((selftMenu) => {
        selfMenuName = selftMenu.data()?.name;
    })
    await firebase.firestore().collection('Menu').where("name", "==", menu.name).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }else{
            doc.forEach((item)=>{
                if(item.data().name.trim() === selfMenuName){
                    check = false;
                }else{
                    check = true;
                }
            })
        }
       
    });
    if (check === true) {
        return (false);
    }else{
        await firebase.storage().ref(`images/${image.name}`).put(image);
        await firebase.storage().ref('images').child(image.name).getDownloadURL().then(url => {
            menu.urlImage = url;
        })
        await dbMenu().doc(`${idMenu}`).update({
            name: menu.name,
            type: menu.type,
            price: menu.price,
            imageName: menu.imageName,
            urlImage: menu.urlImage,
        })
        return (true);
    }
   


}

export const createMenuItem = async (menu: Menu, image: any) => {
    let check = true;
    await firebase.firestore().collection('Menu').where("name", "==", menu.name).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }
    });


    if (check === true) {
        return (false)

    } else {
        await firebase.storage().ref(`images/${image.name}`).put(image);
        await firebase.storage().ref('images').child(image.name).getDownloadURL().then(url => {
            menu.urlImage = url;
        })
        await dbMenu().add({
            name: menu.name,
            price: menu.price,
            type: menu.type,
            imageName: menu.imageName,
            urlImage: menu.urlImage,
            stock: 0,
        })
        return (true)
    }


}


export const deleteMenu = async (menuDelete: Menu) => {
    try {
        await dbMenu().doc(menuDelete.id).delete();
    } catch (e) {
        console.log(e);
    }
}   

export const updateStock = async  (menuItem: Menu, qtyNew: number) => {
    await dbMenu().doc(`${menuItem.id}`).update({
        stock: qtyNew
    })
}
