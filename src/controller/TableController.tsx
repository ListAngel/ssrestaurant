import { Tables, dbTable } from '../model/Tables';

export const getAllTable = async () => {
    const result: Tables[] = []
    await dbTable()
        .get()
        .then((querySnapShot) => {
            querySnapShot.forEach((doc) => {
                const tableItems: Tables = {
                    id: doc.id,
                    tableNumber: doc.data().tableNumber,
                    status: doc.data().status,
                    type: doc.data().type,
                    capacity: doc.data().capacity,

                }
                result.push(tableItems);
            })
        })
    return result;
}
export const getAllTableAvailable = async () => {
    const result: Tables[] = []
    await dbTable()
    .get()
    .then((querySnapShot) => {
        querySnapShot.forEach((doc) => {
            if(doc.data().status === "Available"){
                const tableItems: Tables = {
                    id: doc.id,
                    tableNumber: doc.data().tableNumber,
                    status: doc.data().status,
                    type: doc.data().type,
                    capacity: doc.data().capacity,
    
                }
                result.push(tableItems);
            }
            
        })
    })
return result;
}
export const getTableWithId = async (tableId: string) => {
    let result: Tables = {} as Tables;
    await dbTable().doc(`${tableId}`).get().then((doc) => {
        const newTabel: Tables = {
            id: tableId,
            tableNumber: doc.data()?.tableNumber,
            status: doc.data()?.status,
            type: doc.data()?.type,
            capacity: doc.data()?.capacity,
        }
        result = newTabel
    })
    return result;
}
export const createTable = async (tables: Tables) => {
    let check = true;
    await dbTable().where('tableNumber', '==', tables.tableNumber).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }
    });
    if (check === true) {
        return (false)
    } else {
        await dbTable().add({
            tableNumber: tables.tableNumber,
            type: tables.type,
            capacity: tables.capacity,
            status: tables.status,
        })
        return (true)
    }
}
export const updateTable = async (tableId: string, tables: Tables) => {
    let check = true;
    let selfTable = "";
    await dbTable().doc(`${tableId}`).get().then((result) =>{
        selfTable = result.data()?.tableNumber;
    })
    await dbTable().where('tableNumber', '==', tables.tableNumber).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }else{
            doc.forEach((item) => {
                if(item.data().tableNumber.trim() === selfTable){
                    check = false;
                }else{
                    check = true;
                }
            })
        }
    });
    if (check === true) {
        return (false)
    } else {
        await dbTable().doc(`${tableId}`).update({
            tableNumber: tables.tableNumber,
            type: tables.type,
            capacity: tables.capacity,
            status: tables.status,
        })
        return (true)
    }
}

export const deleteTable = async (tables: Tables) => {
    try {
        await dbTable().doc(tables.id).delete();
    } catch (e) {
        console.log(e);
    }
}

export const updateStatusTable = async  (idTables: string, statusNew: string) => {
    await dbTable().doc(`${idTables}`).update({
        status: statusNew
    })
}
