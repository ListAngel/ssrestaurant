import {Purchasing, dbPurchasing} from '../model/Purchasing';
import {updateStock} from '../controller/MenuController';

export const getAllPurchasing = async () => {
    const result: Purchasing[] = [];
    await dbPurchasing()
        .get()
        .then((querySnapShot) => {
            querySnapShot.forEach((doc) => {
                const data = doc.data();
                const purchasingItem: Purchasing = {
                    id: doc.id,
                    vendorName: data.vendorName,
                    materialPurchasing: data.materialPurchasing,
                    status: data.status,
                    date: data.date,
                    total: data.total
                }
                result.push(purchasingItem);
            })
        })
    return result;
}

export const getPurchasingWithId = async (purchasingId: string) => {
    let result: Purchasing = {} as Purchasing;
    await dbPurchasing()
        .doc(`${purchasingId}`)
        .get()
        .then((doc) => {
            const data = doc.data();
            const purchasingItem: Purchasing = {
                id: purchasingId,
                vendorName: data?.vendorName,
                materialPurchasing: data?.materialPurchasing,
                status: data?.status,
                total: data?.total,
                date: data?.date
            }
            result = purchasingItem;
        })
    return result;
}

export const updatePurchasingItem = async (idPurchasing: string,purchasing: Purchasing) => {
    try{
        await dbPurchasing().doc(`${idPurchasing}`).update({
            vendorName: purchasing.vendorName,
            materialPurchasing: purchasing.materialPurchasing,
            status: purchasing.status,
            total: purchasing.total,
            date: purchasing.date,
        })
        
        return true
    }catch(e){
        console.log(e)
        return false
    }
}
export const createPruchasingItem = async (purchasing: Purchasing) => {
    try{
        await dbPurchasing().add({
            vendorName: purchasing.vendorName,
            materialPurchasing: purchasing.materialPurchasing,
            status: purchasing.status,
            total: purchasing.total,
            date: purchasing.date,
        })
       
        return (true)
    }catch(e){
        return false;
    }

}
export const deletePurchasing = async (purhasingDelete: Purchasing) => {
    try {
        await dbPurchasing().doc(purhasingDelete.id).delete();
        return true
    } catch (e) {
        console.log(e);
        return false
    }
}   

export const receiptGoodPruchasing = async (purchasingSelected: Purchasing) => {
    try{
        await dbPurchasing().doc(purchasingSelected.id).update({
            status: "done"
        });
        await purchasingSelected.materialPurchasing.forEach((doc) => {
            updateStock(doc.menu,doc.menu.stock+doc.quantity);
        });
        return true
    }catch (e){
        console.log(e);
        return false
    }
}
