import React from 'react';
import { Reservation, dbReservation } from '../model/Reservation';
import { updateStatusTable } from '../controller/TableController';

export const getAllReservation = async () => {
    const result: Reservation[] = []
    await dbReservation()
        .get()
        .then((querySnapShot) => {
            querySnapShot.forEach((doc) => {
                const reservationItem: Reservation = {
                    id: doc.id,
                    pic: doc.data().pic,
                    tables: doc.data().tables,
                    reservationCreated: doc.data().reservationCreated,
                    reservationDate: doc.data().reservationDate,
                    status: doc.data().status,

                }
                result.push(reservationItem);
            })
        })
    return result;
}
export const getReservationWithId = async (reservationId: string) => {
    let result: Reservation = {} as Reservation;
    await dbReservation().doc(`${reservationId}`).get().then((doc) => {
            const data = doc.data();
            const reservationItem: Reservation = {
                id: doc.id,
                pic: data?.pic,
                tables: data?.tables,
                reservationCreated: data?.reservationCreated,
                reservationDate: data?.reservationDate,
                status: data?.status,

            }
            result = reservationItem;

        })
        console.log(result);
    return result;
}
export const updateReservation = async (reservationId: string, reservation: Reservation) => {
    let check = true;
    let selfRervation = "";
    await dbReservation().doc(`${reservationId}`).get().then((result) =>{
        selfRervation = result.id;
    })
    const checkReservation = await getAllReservation();
    if (checkReservation.length > 0){
        checkReservation.forEach((item) => {
            if(item.id !== selfRervation){
                if(item.tables.id === reservation.tables.id){
                    if (reservation.reservationDate <= item.reservationDate+3600000){
                        check = false;
                    }    
                }
            }
        })
    }
    if(check === true){
        await dbReservation().doc(`${reservationId}`).update({
            pic: reservation.pic,
            tables: reservation.tables,
            reservationCreated: reservation.reservationCreated,
            reservationDate: reservation.reservationDate,
            status: reservation.status,
        })
        return (true)
    }else {
        return false;
    }
}
export const createReservation = async (reservation: Reservation) => {
    let check = true;
    const checkReservation = await getAllReservation();
    if (checkReservation.length > 0){
        checkReservation.forEach((item) => {
            if(item.tables.id === reservation.tables.id){
                if (reservation.reservationDate <= item.reservationDate+3600000){
                    check = false;
                }    
            }
        })
    }
    if(check === true){
        await dbReservation().add({
            pic: reservation.pic,
            tables: reservation.tables,
            reservationCreated: reservation.reservationCreated,
            reservationDate: reservation.reservationDate,
            status: reservation.status,
        })
        
        return (true)
    }else {
        return false;
    }
}
export const deleteReservation = async (reservation: Reservation) => {
    try {
        await dbReservation().doc(reservation.id).delete();
    } catch (e) {
        console.log(e);
    }
}

export const updateStatusReservation = async (reservation: Reservation, statusNew: string) => {
    try {
        await dbReservation().doc(`${reservation.id}`).update({
            status: statusNew
        })
    } catch (e) {
        console.log(e);
    }
}