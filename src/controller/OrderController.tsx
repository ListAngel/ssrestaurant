import { Order, dbOrder } from '../model/Order';
import {Promo} from '../model/Promo';
import { updateStock } from '../controller/MenuController';
import {updateStatusTable} from '../controller/TableController';
import {updateStatusReservation} from '../controller/ReservationController';
import {getMenuById} from '../controller/MenuController';
export const getAllOrder = async () => {
    const result: Order[] = [];
    await dbOrder()
    .get()
    .then((querySnapShot) => {
        querySnapShot.forEach((doc) => {
            const data = doc.data();
            const orderItem: Order = {
                id: doc.id,
                pic: data.pic,
                menuOrder: data.menuOrder,
                table: data.table,
                status: data.status,
                date: data.date,
                promo: data.promo,
                total: data.total,
                grandTotal: data?.grandTotal,
                reservation: data?.reservation,
            }
            result.push(orderItem);
        })
    })
    return result;
}

export const getOrderWithId = async (orderId: string) => {
    let result: Order = {} as Order;
    await dbOrder()
        .doc(`${orderId}`)
        .get()
        .then((doc) => {
            const data = doc.data();
            const orderItem: Order = {
                id: doc.id,
                pic: data?.pic,
                menuOrder: data?.menuOrder,
                table: data?.table,
                status: data?.status,
                date: data?.date,
                promo: data?.promo,
                total: data?.total,
                grandTotal: data?.grandTotal,
            }
            result = orderItem;
        })
    return result;
}

export const updateOrder = async (idOrder: string,order: Order, oldOrder: Order) => {
    try{
        
        await oldOrder.menuOrder.forEach((doc) => {
            getMenuById(String(doc.menu.id)).then((item) => {
                console.log("Qyt", item.stock, doc.quantity);
                updateStock(doc.menu,Number(item.stock+doc.quantity));
            })
        })

        await updateStatusTable(String(oldOrder.table.id), "Available");

        await dbOrder().doc(`${idOrder}`).update({
            pic: order.pic,
            menuOrder: order.menuOrder,
            table: order.table,
            status: order.status,
            date: order.date,
            total: order.total,
        })
        await order.menuOrder.forEach((doc) => {
            updateStock(doc.menu,doc.menu.stock-doc.quantity);
        })

        await updateStatusTable(String(order.table.id), "Used");
        return true
    }catch(e){
        console.log(e)
        return false
    }
}

export const orderPayment = async (order: Order, newGrandTotal: number, newPromo: Promo) =>{
    try {
        console.log(order.reservation)
        if(order.reservation !== undefined){
            await dbOrder().doc(`${order.id}`).update({
                status: "Reserved",
                grandTotal: newGrandTotal,
                promo: newPromo,
            })
            await updateStatusReservation(order.reservation,"Done");
            
           
        }else{
            await dbOrder().doc(`${order.id}`).update({
                status: "Done",
                grandTotal: newGrandTotal,
                promo: newPromo,
            })
            await updateStatusTable(String(order.table.id), "Available");
        }
       
        return true;
    }catch(e){
        console.log(e);
        return false;
    }
}

export const createOrder = async (order: Order) =>{
    try{
        await dbOrder().add({
            pic: order.pic,
            menuOrder: order.menuOrder,
            table: order.table,
            status: order.status,
            date: order.date,
            total: order.total,
        })
        await order.menuOrder.forEach((doc) => {
            updateStock(doc.menu,doc.menu.stock-doc.quantity);
        })

        await updateStatusTable(String(order.table.id), "Used");
        return (true)
    }catch(e){
        return false;
    }
}

export const createOrderReservation = async (order: Order) => {
    try{
        await dbOrder().add({
            pic: order.pic,
            menuOrder: order.menuOrder,
            table: order.table,
            status: order.status,
            date: order.date,
            total: order.total,
            reservation: order.reservation,
        })
        await order.menuOrder.forEach((doc) => {
            updateStock(doc.menu,doc.menu.stock-doc.quantity);
        })

        if(order.reservation !== undefined){
            await updateStatusReservation(order.reservation,"Reserved Order");
        }
        return (true)
    }catch(e){
        return false;
    }
}

export const deleteOrder = async (orderDelete: Order) =>{
    try {
        await orderDelete.menuOrder.forEach((doc) => {
            updateStock(doc.menu,doc.menu.stock+doc.quantity);
        })

        await updateStatusTable(String(orderDelete.table.id), "Available");
        if(orderDelete.reservation !== undefined){
           await updateStatusReservation(orderDelete.reservation,"Cancel"); 
        }
       
        await dbOrder().doc(orderDelete.id).delete();
        
        return true;
    }catch(e){
        return false;
    }
}
