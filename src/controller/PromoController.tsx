import { Promo, dbPromo } from '../model/Promo';


export const getAllPromo = async () => {
    const result: Promo[] = [];
    await dbPromo()
        .get()
        .then((querySnapShot) => {
            querySnapShot.forEach((doc) => {
                const promoItems: Promo = {
                    id: doc.id,
                    codePromo: doc.data().codePromo,
                    promoTitle: doc.data().promoTitle,
                    startDate: doc.data().startDate,
                    expiredDate: doc.data().expiredDate,
                    discount: doc.data().discount
                }
                result.push(promoItems);
            })
        })
    return result;
}

export const getPromoWithId = async (promoId: string) => {
    let result: Promo = {} as Promo;

    await dbPromo().doc(`${promoId}`).get().then((doc) => {
        const newPromo: Promo = {
            id: doc.id,
            codePromo: doc.data()?.codePromo,
            promoTitle: doc.data()?.promoTitle,
            startDate: doc.data()?.startDate,
            expiredDate: doc.data()?.expiredDate,
            discount: doc.data()?.discount,
        }
        result = newPromo
    })

    return (result);
}

export const createPromo = async (promo: Promo) => {
    let check = true;
    await dbPromo().where('codePromo', '==', promo.codePromo).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }
    });

    if (check === true) {
        return (false)
    } else {
        await dbPromo().add({
            codePromo: promo.codePromo,
            promoTitle: promo.promoTitle,
            startDate: promo.startDate,
            expiredDate: promo.expiredDate,
            discount: promo.discount,
        })
        return (true)
    }
}

export const deletePromo = async (promoDeleted: Promo) => {
    try {
        await dbPromo().doc(promoDeleted.id).delete();
    } catch (e) {
        console.log(e);
    }
}

export const updatePromo = async (promoId: string, promo: Promo) => {
    let check = true;
    let selfPromo = "";
    await dbPromo().doc(`${promoId}`).get().then((result) =>{
        selfPromo = result.data()?.codePromo;
    })
    await dbPromo().where('codePromo', '==', promo.codePromo).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }else{
            doc.forEach((item) => {
                if(item.data().codePromo.trim() === selfPromo){
                    check = false;
                }else{
                    check = true;
                }
            })
        }
    });

    if (check === true) {
        return (false)
    } else {
        await dbPromo().doc(`${promoId}`).update({
            codePromo: promo.codePromo,
            promoTitle: promo.promoTitle,
            startDate: promo.startDate,
            expiredDate: promo.expiredDate,
            discount: promo.discount,
        })
        return (true)
    }
}

export const checkPromo = async (codePromo: string) =>{
    let result: Promo = {} as Promo;;
    let check: boolean = true;
    const today = new Date();
    await dbPromo().where('codePromo', '==', codePromo).get().then((doc) => {
        if (doc.docs.length === 0) {
            check = false;
        }else {
            doc.forEach((item) => {
                if(item.data().expiredDate < today.getTime() || item.data().startDate > today.getTime()){
                    check = false;
                }else{
                    const newPromo: Promo = {
                        id: item.id,
                        codePromo: item.data()?.codePromo,
                        promoTitle: item.data()?.promoTitle,
                        startDate: item.data()?.startDate,
                        expiredDate: item.data()?.expiredDate,
                        discount: item.data()?.discount,
                    }
                    result = newPromo;
                }
            }) 
        }
    })
    console.log(check);
    if(check === true){
        return (result);
    }else {
        return 0;
    }
}