import firebase from 'firebase';
export interface Menu {
    id?: string;
    name: string;
    price: number;
    type: string;
    imageName: string;
    urlImage: string;
    stock: number;
}

export function dbMenu(){
    return firebase.firestore().collection("Menu");
}
