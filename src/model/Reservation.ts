import firebase from 'firebase';
import {Tables} from './Tables';

export interface Reservation{
    id?: string;
    pic: string;
    tables: Tables;
    reservationCreated: number;
    reservationDate: number;
    status: string;
}

export function dbReservation(){
    return firebase.firestore().collection('Reservation');
} 