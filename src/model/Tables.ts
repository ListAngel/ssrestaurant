import firebase from 'firebase';

export interface Tables {
    id?: string;
    tableNumber: string;
    type: string;
    capacity: number;
    status: string;
}

export function dbTable() {
    return firebase.firestore().collection("Table");
}