import {Menu} from './Menu';

export interface MaterialPurchasing{
    menu: Menu;
    quantity: number;
    unitPrice: number;
}