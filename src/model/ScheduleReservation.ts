import firebase from 'firebase';
import {Tables} from './Tables';
import {Reservation} from './Reservation';

export interface ScheduleReservation {
    id?: string;
    tables: Tables;
    reservation: Reservation[];
}