import {MenuOrder} from './MenuOrder';
import {Tables} from './Tables';
import {Promo} from './Promo';
import {Reservation} from './Reservation';
import firebase from 'firebase';

export interface Order{
    id?: string;
    pic: string;
    menuOrder: MenuOrder[];
    table: Tables;
    status: string;
    date: number;
    promo: Promo;
    total: number;
    grandTotal: number;
    reservation?: Reservation;

}
export function dbOrder(){
    return firebase.firestore().collection("Order");
}
