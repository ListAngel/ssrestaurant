import firebase from 'firebase';

export interface Promo {
    id?: string;
    codePromo: string;
    promoTitle: string;
    startDate: number;
    expiredDate: number;
    discount: number;
}

export function dbPromo(){
    return firebase.firestore().collection("Promo");
}
