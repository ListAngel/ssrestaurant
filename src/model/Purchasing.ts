import firebase from 'firebase';
import {MaterialPurchasing} from './MaterialPurchasing';
export interface Purchasing {
    id?: string;
    vendorName: string;
    materialPurchasing: MaterialPurchasing [];
    status: string;
    total: number;
    date: number;

}


export function dbPurchasing(){
   return firebase.firestore().collection('Purchasing');
} 